<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }
    public function phone_click() {
        if( have_rows('events', 'options') ):
            while ( have_rows('events', 'options') ) : the_row();
                if( get_row_layout() == 'event_tel' ): 
                      if ( get_sub_field('manual') === false) :
                        $phone_event = "gtag('event', 'click', {'event_category': 'button', 'event_label': '".get_sub_field('label')."'});ym(".get_sub_field('id').", 'reachGoal', '".get_sub_field('label')."'); return true;";
                      else: 
                        $phone_event = get_sub_field('event_tel');
                      endif;
                endif;
            endwhile;
            endif;
        return $phone_event;
    }
}
