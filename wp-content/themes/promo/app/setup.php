<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {

    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_style( 'ProximaNova',  get_template_directory_uri() . '/assets/fonts/fonts.css', false ); 
    wp_register_script( 'sage/frontpage.js', asset_path('scripts/frontpage.js'),  false, null, true);
    wp_register_script( 'sage/swiper.js', asset_path('scripts/swiper.js'),  false, null, true);
    if(is_front_page() ){
        wp_enqueue_script( 'sage/frontpage.js');
    }
    if (is_page( get_field("p_services", "options")) || is_page(get_field("p_about", "options")) || is_page(get_field("p_reviews", "options")) || is_archive() ) {
        wp_enqueue_script( 'sage/swiper.js');
    }
    wp_dequeue_script('contact-form-7');
    wp_dequeue_style('contact-form-7');
    // wp_dequeue_script('google-recaptcha');
    // wp_dequeue_style('google-recaptcha');
    // wp_dequeue_style('recaptcha');
    if(get_field('google_id', 'options')) {
       
        wp_register_script( 'gtag1', 'https://www.google-analytics.com/analytics.js_asyncload_', '', null, true);
        wp_register_script( 'gtag2', 'https://www.googletagmanager.com/gtag/js?id='.get_field("google_id", "options").'_asyncload_', '', null, true);
        wp_enqueue_script('gtag1');
        wp_enqueue_script('gtag2');
    }
    if(get_field('venyoo_id', 'options')) {
        wp_register_script( 'venyoo' , '//api.venyoo.ru/wnew.js?wc=venyoo/default/science&widget_id='.get_field('venyoo_id', 'options'), array(), null, true);
        wp_enqueue_script('venyoo');
    }
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), false, null, true);
    if (get_field('_metrika', 'options')) {
        foreach (get_field('_metrika', 'options') as $key => $value) {
            if($value['place'] === 'header') {
                wp_register_script('metrika1'.$key,'', [], null, true);
                wp_enqueue_script('metrika1'.$key);
                wp_add_inline_script( 'metrika1'.$key, $value['code'] );
            }
            if($value['place'] === 'footer') {
                wp_register_script('metrika2'.$key,'', [], null, true);
                wp_enqueue_script('metrika2'.$key);
                wp_add_inline_script( 'metrika2'.$key, $value['code'] );
            }
        }
    }

        if( have_rows('events', 'options') ):
            $counter = 0;
            while ( have_rows('events', 'options') ) : the_row();
                if( get_row_layout() == 'event_submit' ): 
                    if ( get_sub_field('manual') === false) :
                        wp_register_script('events1'.$counter,'', [], null, true);
                        wp_enqueue_script('events1'.$counter);
                        wp_add_inline_script( 'events1'.$counter, "document.addEventListener( 'wpcf7mailsent', function( event ) { console.log('event fired'); gtag('event', 'send', {'event_category': 'form', 'event_label': '".get_sub_field('label')."'});ym(".get_sub_field('id').", 'reachGoal', '".get_sub_field('label')."');}, false);" );
                    else: 
                        wp_register_script('events2'.$counter,'', [], null, true);
                        wp_enqueue_script('events2'.$counter);
                        wp_add_inline_script( 'events2'.$counter, "document.addEventListener( 'wpcf7mailsent', function( event ) {console.log('event fired');".get_sub_field('event_manual')."});" );
                    endif;
                endif;
                if( get_row_layout() == 'event_tel' ): 
                    if ( get_sub_field('manual') === false) :
                      $script = "var classname = document.getElementsByClassName('phone-number');for (var i = 0; i < classname.length; i++) {classname[i].addEventListener('click', function() {gtag('event', 'click', {'event_category': 'button', 'event_label': '".get_sub_field('label')."'});ym(".get_sub_field('id').", 'reachGoal', '".get_sub_field('label')."'); return true;});}";
                      wp_register_script('events3'.$counter,'', [], null, true);
                      wp_enqueue_script('events3'.$counter);
                      wp_add_inline_script( 'events3'.$counter, $script );
                    else: 
                      $script = "var classname = document.getElementsByClassName('phone-number'); for (var i = 0; i < classname.length; i++) {classname[i].addEventListener('click', function(){".get_sub_field('event_tel')."});}";
                      wp_register_script('events4'.$counter,'', [], null, true);
                      wp_enqueue_script('events4'.$counter);
                      wp_add_inline_script( 'events4'.$counter, $script );
                    endif;
                endif;
                $counter++;
            endwhile;
        endif;
   
    wp_enqueue_script('contact-form-7','', ['jquery'], null, true);
    wp_enqueue_style('contact-form-7');
    // wp_enqueue_script('google-recaptcha','', array(), null, true);
    
    // if (is_single() && comments_open() && get_option('thread_comments')) {
    //     wp_enqueue_script('comment-reply');
    // }
}, 100);


/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    // add_theme_support('soil-clean-up');
    // add_theme_support('soil-jquery-cdn');
    // add_theme_support('soil-nav-walker');
    // add_theme_support('soil-nice-search');
    // add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
    sage('blade')->compiler()->component('components.icon','icon');
});
