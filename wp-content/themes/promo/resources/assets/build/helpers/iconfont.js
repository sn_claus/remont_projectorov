const webfontsGenerator = require('webfonts-generator');
const fs = require('fs');
const path = require('path');
const _ = require('underscore');
const config = require('../config');

const SRC = path.join(__dirname, '../../icons/icon_font/icons');
const FILES = _.map(fs.readdirSync(SRC), function(file) {
  return path.join(SRC, file)
});

const TYPES = ['woff2', 'woff'];
const NAME = 'svgfont';
const OPTIONS = {
  dest: config.paths.assets + '/fonts/' + NAME,
  files: FILES,
  fontName: NAME,
  normalize: true,
  fontHeight: 1001,
  types: TYPES,
  order: TYPES,
  cssDest: (config.paths.assets + '/styles/_helpers/' + '_' + NAME + '.sass'),
  cssTemplate: config.paths.assets + '/icons/icon_font/template.hbs',
};

webfontsGenerator(OPTIONS, function(error) {
  if (error) console.log('Fail!', error)
  else console.log('Done!')
});