export default function bootstrap(App) {
	const app = new App();
	app.registerComponents();

	document.addEventListener('DOMContentLoaded', () => app.initialize());
}
