import Component from '../components';

export default class cf7 extends Component {
    static get selector() {
      return '.form-id';
    }
    constructor(host) {
        super(host);
        this.element = document.getElementById('form_id');
        if(document.body.contains(this.element)) {
            this.value = document.getElementById('p_title').textContent;
            this.element.value = this.value;
        }
    }
}