import Component from '../components';
import TweenLite from 'gsap';

export default class countdown extends Component {
    static get selector() {
      return '.timer';
    }
    constructor(host) {
        super(host);
        TweenLite.defaultEase = Expo.easeOut;
         if(!t) {
            var t = getCookie('timer');
         }
         // other ways --> "0:15" "03:5" "5:2"
       if(t) {
         t = getCookie('timer');
         if(t === '00:00:00' || t === '00:00:01' || parseInt(t) == 23) {
            t = "02:00:00";
            countdownFinished();
            var date2 = new Date(new Date().getTime() + 3600 * 2000);
            document.cookie = "date="+date2;
            var localdate2 = getCookie('date');
            document.cookie = "timer=02:00:00; path=/; expires=" + localdate2;
         }
        }
        else {
         t = "02:00:00";
         var date = new Date(new Date().getTime() + 3600 * 2000);
         document.cookie = "date="+date;
         var localdate = getCookie('date');
         document.cookie = "timer=02:00:00; path=/; expires=" + localdate;

        }
        var reloadBtn = document.querySelector('.reload');
        var timerEl = document.querySelector('.timer');
        
           
         function updateTimerCockie() {
            document.cookie = "timer="+time.hours +':'+ time.min+':'+ time.sec+"; path=/; expires=" + getCookie('date');
         }

           var self = this,
               timerEl = document.querySelector('.timer'),
               hoursGroupEl = timerEl.querySelector('.hours-group'),
               minutesGroupEl = timerEl.querySelector('.minutes-group'),
               secondsGroupEl = timerEl.querySelector('.seconds-group'),
        
               hoursGroup = {
                  firstNum: hoursGroupEl.querySelector('.first'),
                  secondNum: hoursGroupEl.querySelector('.second')
               },
               minutesGroup = {
                  firstNum: minutesGroupEl.querySelector('.first'),
                  secondNum: minutesGroupEl.querySelector('.second')
               },
        
               secondsGroup = {
                  firstNum: secondsGroupEl.querySelector('.first'),
                  secondNum: secondsGroupEl.querySelector('.second')
               };
        
           var time = {
              hours: t.split(':')[0],
              min: t.split(':')[1],
              sec: t.split(':')[2]
           };
        
           var timeNumbers;
        
           function updateTimer() {
              
            if(time === {hours: '00', min: '00', sec: '00'} || time === {hours: '00', min: '00' ,sec: '00'} || time.hours === '23') {
               time = {hours: '00', min: '00' ,sec: '00'};
               t = '00:00:00';
               // countdownFinished();
               updateTimerCockie();
               TweenMax.to(timerEl, 1, { opacity: 1 });
               return;
            }
              var timestr;
              var date = new Date();
        
              date.setHours(time.hours);
              date.setMinutes(time.min);
              date.setSeconds(time.sec);
        
              var newDate = new Date(date.valueOf() - 1000);
              var temp = newDate.toTimeString().split(" ");
              var tempsplit = temp[0].split(':');

              time.hours = tempsplit[0];
              time.min = tempsplit[1];
              time.sec = tempsplit[2];
        
              timestr = time.hours + time.min + time.sec;
              timeNumbers = timestr.split('');
              updateTimerDisplay(timeNumbers);
        
              if(timestr === '000000') {
                 countdownFinished();
               
              }

              if(timestr != '000000')
                 setTimeout(updateTimer, 1000);
                 setTimeout(updateTimerCockie, 1000);
        
           }
        
           function updateTimerDisplay(arr) {
              animateNum(hoursGroup.firstNum, arr[0]);
              animateNum(hoursGroup.secondNum, arr[1]);
              animateNum(minutesGroup.firstNum, arr[2]);
              animateNum(minutesGroup.secondNum, arr[3]);
              animateNum(secondsGroup.firstNum, arr[4]);
              animateNum(secondsGroup.secondNum, arr[5]);
        
           }
        
           function animateNum (group, arrayValue) {
        
              TweenMax.killTweensOf(group.querySelector('.number-grp-wrp'));
              TweenMax.to(group.querySelector('.number-grp-wrp'), 1, {
                 y: - group.querySelector('.num-' + arrayValue).offsetTop
              });
        
           }
           

           var Timeout = setTimeout(updateTimer, 1000);
           var TimeoutC = setTimeout(updateTimerCockie, 1000);
         function countdownFinished() {
            clearTimeout(Timeout);
            clearTimeout(TimeoutC);
            }
        
        
        
        function getCookie(name) {
         var matches = document.cookie.match(new RegExp(
           "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
         ));
         return matches ? decodeURIComponent(matches[1]) : undefined;
       }
        // reloadBtn.addEventListener('click', function () {
        //    TweenMax.to(this, 0.5, { opacity: 0, onComplete:
        //       function () { 
        //          reloadBtn.style.display= "none";
        //       } 
        //    });
        //    TweenMax.to(timerEl, 1, { opacity: 1 });
        //    initTimer("02:00:00");
        // });
    }
    
    initialize() {

    }
  }
  

