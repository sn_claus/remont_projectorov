import Component from '../components';
import lozad from 'lozad';

export default class Lazyload extends Component {
    static get selector() {
      return '.lazyload';
    }
    constructor(host) {
        super(host);
        const observer = lozad('.lazyload');
        observer.observe();
    }
    
    initialize() {

    }
  }
  