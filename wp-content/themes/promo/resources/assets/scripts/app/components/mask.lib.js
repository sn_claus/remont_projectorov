import Component from '../components';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import IMask from 'imask';
export default class mask extends Component {
    static get selector() {
      return 'input[type="tel"]';
    }
    constructor(host) {
        super(host);
        var phoneMask = IMask(
            this.host, {
                mask: '+{7} (000) 000-00-00'
            });
    }
}