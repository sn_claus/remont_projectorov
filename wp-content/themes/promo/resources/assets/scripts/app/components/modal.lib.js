import Component from '../components';
import MicroModal from 'micromodal';

export default class Modal extends Component {
    static get selector() {
      return '.micro_modal';
    }
    constructor(host) {
      super(host);
      this.config = {
        disableScroll: true,
        disableFocus: true,
        awaitCloseAnimation: true,
      };
    }
    
    initialize() {
      MicroModal.init(this.config);
    }
  }

