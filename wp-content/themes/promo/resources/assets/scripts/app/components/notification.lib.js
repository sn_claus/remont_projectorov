import Component from '../components';
import {MDCSnackbar} from '@material/snackbar';

export default class notification extends Component {
    static get selector() {
        return 'body';
      }
      
      constructor(host) {
        super(host);
        this.snackbar = new MDCSnackbar(document.querySelector('.mdc-snackbar'));
        document.addEventListener( 'wpcf7invalid', this.showLabel.bind(this));
        document.addEventListener( 'wpcf7spam', this.showLabel.bind(this));
        document.addEventListener( 'wpcf7mailfailed', this.showLabel.bind(this));
        document.addEventListener( 'wpcf7mailsent', this.showLabel.bind(this));
        document.addEventListener( 'wpcf7submit', this.showLabel.bind(this));
       }
      showLabel(event) {
        this.snackbar.labelText=event.detail.apiResponse.message;
        this.snackbar.open();
      }
      initialize() {

      }
}