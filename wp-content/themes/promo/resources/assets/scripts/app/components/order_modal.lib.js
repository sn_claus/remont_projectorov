import Component from '../components';

export default class order_modal extends Component {
    static get selector() {
      return '.order-modal';
    }
    constructor(host) {
        super(host);
        // this.btns = this.find('button, .btn-info');
        // if(document.body.contains(this.element)) {
        //     this.value = document.getElementById('p_title').textContent;
        //     this.element.value = this.value;
        // }
        this.createEvent(this.host, 'click', this.toform.bind(this));

    }
    toform() {
        // title = this.dataset;
        this.element = document.getElementById('form_id2');
        this.title = this.host.dataset.title;
        this.element.value = this.title;
    }
}