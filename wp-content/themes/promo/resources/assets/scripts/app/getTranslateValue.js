/**
 * @typedef {Object} Translate
 * @property {number} x The X Coordinate
 * @property {number} y The Y Coordinate
 */

/**
 *
 * @param {HTMLElement} node
 * @returns {Translate}
 */
export default function getTranslateValue(node) {
    const { transform } = getComputedStyle(node);
    const matrix = transform.match(/^matrix\((.+)\)$/);
  
    if (!matrix) {
      return {
        x: 0,
        y: 0
      };
    }
  
    const values = matrix[1].split(', ');
  
    return {
      x: parseFloat(values[4]),
      y: parseFloat(values[5])
    };
  }
  