import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import toArray from 'lodash/toArray';
import listen from 'dom-helpers/events/listen';

export function find(selector, context) {
    const nodes = toArray(document.querySelectorAll(selector));

    if (typeof nodes[0] === 'undefined') {
      return null;
    }

    return nodes.length > 1 ? nodes : nodes[0];
  }

  /**
   * Attach event to DOM node
   * @param {(string|HTMLElement|Array<HTMLElement>)} target
   * @param {string} eventName
   * @param {Function} handler
   * @returns {Function} Function that calls "off" hook
   */
  export function createEvent(target, eventName, handler) {
    
    target = isString(target)
      ? toArray(document.querySelectorAll(target))
      : target;

    const listeners = isArray(target)
      ? target.map(node => listen(node, eventName, handler))
      : [listen(target, eventName, handler)];

    return () => listeners.forEach(off => off());
  }
