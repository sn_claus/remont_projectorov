import {createEvent, find} from "./eventbus.lib";
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import hasClass from 'dom-helpers/class/hasClass';

export function form_init() {
      var host = find('.form-init:not(.simulate)', document);
      var inputs = find('.input-wrap input:not([type="submit"]),.form-init textarea', host);
      var checks = find('.simple-group.checkbox', host);
      createEvent(inputs, 'focusin', focusIn.bind(this));
      createEvent(inputs, 'focusout', focusOut.bind(this));
      createEvent(inputs, 'change', focusOut.bind(this));
      createEvent(checks, 'click', function(){if(hasClass(this, 'invalid')){removeClass(this, 'invalid')}});
      document.addEventListener( 'wpcf7invalid', invalid.bind(this));
      document.addEventListener( 'wpcf7submit', formCheck.bind(this));
    }

    function focusIn({ target }) {
        var input = target.closest('.input-wrap');
        addClass(input, 'focused');
        removeClass(input, 'invalid');
    }
    function invalid() {
        var inv = document.querySelectorAll('.wpcf7-not-valid');
        for (var i = 0, len = inv.length; i < len; i++) {
            var input = inv[i].closest('.input-wrap');
            addClass(input, 'filled');
            addClass(input, 'invalid');
        }
        
    }
    function formCheck() {
        // elem = target.find('form');
        var inv = document.querySelectorAll('.wpcf7-form.unaccepted');
        // console.log(hasClass(elem, 'unaccepted'));
        for (var i = 0, len = inv.length; i < len; i++) {
            var input = inv[i].querySelector('.simple-group.checkbox');
            addClass(input, 'invalid');
        }
    }
    function focusOut({ target }) {
        if(!target.value) {
            var input = target.closest('.input-wrap');
            removeClass(input, 'focused');
            removeClass(input, 'filled');
        }
        else if(target.value) {
            var input = target.closest('.input-wrap');
            removeClass(input, 'focused');
            addClass(input, 'filled');
        }
    }
   

