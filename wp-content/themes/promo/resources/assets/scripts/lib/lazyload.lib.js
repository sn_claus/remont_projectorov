import lozad from 'lozad';

export function lozad_init() {
  const observer = lozad('.lazyload');
  observer.observe();
}