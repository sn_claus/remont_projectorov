import IMask from 'imask';
export function mask_init() {
    var elements = document.querySelectorAll('input[type="tel"]');
    [].forEach.call(elements, function(element) {
       IMask(element, {mask: '+{7} (000) 000-00-00'});
    }); 
}