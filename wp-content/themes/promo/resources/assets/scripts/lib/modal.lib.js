import MicroModal from 'micromodal';

export function modal_init() {
  var config = {
    disableScroll: true,
    disableFocus: true,
    awaitCloseAnimation: true,
  };
  MicroModal.init(config);
}



    