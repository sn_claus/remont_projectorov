// import Component from '../components';
// import createEventBus from '../eventBus';
import {createEvent} from "./eventbus.lib";
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import hasClass from 'dom-helpers/class/hasClass';


var siteMenuActive = false;
export function nav_init() {
  
   var element = document.querySelector('.nav-menu-toggler');
    createEvent(element, 'click', toggleMenu);
    // element.addEventListener('tap', toggleMenu());
    // var links = document.querySelectorAll('.nav-mobile a');
    // for (var i = 0, len = links.length; i < len; i++) {
    //   links[i].addEventListener('click', function(event) {
    //     document.querySelector('.nav-menu-toggler button').click();
    //   });
    // }
    window.addEventListener('scroll', scroll_bg);

  }
  function toggleMenu() {
    console.log(this);
    // var siteMenuActive = false;
    var host = document.querySelector(".main-nav");
    var trigger = document.querySelector('.nav-menu-toggler button');
    var mainNav = document.querySelector('footer .nav-mobile');
    if (siteMenuActive) {
      removeClass(trigger, 'opened');
      removeClass(document.body, 'nav-open');
      removeClass(host, 'is-site-menu-active');
      removeClass(mainNav, 'is-sub-nav-active');
    } else {
      addClass(trigger, 'opened');
      addClass(host, 'is-site-menu-active');
      addClass(document.body, 'nav-open');
    }

    siteMenuActive = !siteMenuActive;
  }
  function scroll_bg () {
    // const headerHeight = this.header.offsetHeight;
    const viewportTop = scrollY || pageYOffset;
    var header = document.querySelector('header');
    // const sectionTop = section.offsetTop - headerHeight / 2;
    // const sectionBottom = sectionTop + section.offsetHeight;
    if (viewportTop >= innerHeight && !hasClass(header, 'bg')) {
      addClass(header, 'bg');
      
    } else if (viewportTop < innerHeight && hasClass(header, 'bg')) {
      removeClass(header, 'bg');
      removeClass(header, 'hide');
    }
  }

  