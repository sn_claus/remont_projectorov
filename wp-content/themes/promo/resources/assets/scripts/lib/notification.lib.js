import {MDCSnackbar} from '@material/snackbar';

export function notification_init() {
        document.addEventListener( 'wpcf7invalid', showLabel.bind(this));
        document.addEventListener( 'wpcf7spam', showLabel.bind(this));
        document.addEventListener( 'wpcf7mailfailed', showLabel.bind(this));
        document.addEventListener( 'wpcf7mailsent', showLabel.bind(this));
        document.addEventListener( 'wpcf7submit', showLabel.bind(this));
     
}
function showLabel(event) {
  var snackbar = new MDCSnackbar(document.querySelector('.mdc-snackbar'));
  snackbar.labelText=event.detail.apiResponse.message;
  snackbar.open();
}