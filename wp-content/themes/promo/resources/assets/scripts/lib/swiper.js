// import "swiper/dist/css/swiper.min.css";
import Swiper from 'swiper/dist/js/swiper.js';

export function updateSwiper() {
  var mobile_only = undefined;
  var container = document.querySelector('.swiper-container');
  // if(this.media.matches && this.mobile_only == undefined) {      
    var swiper1 = document.querySelector('.carousel .swiper-container');
    var swiper2 = document.querySelector('.masters-carousel.swiper-container');
    var swiper3 = document.querySelector('.swiper-container.reviews-swiper');
    if(document.body.contains(swiper3)) {  
      mobile_only = new Swiper('.swiper-container.reviews-swiper', {            
          slidesPerView: 'auto',
          spaceBetween: 25,
          direction: 'vertical',
          freeMode: true,
          draggable: true,
          mousewheel: {
            releaseOnEdges: true,
            sensitivity: 0.75
          },
          touchReleaseOnEdges: true,
          scrollbar: {
            el: '.swiper-scrollbar',
            draggable: true,
            snapOnRelease: false
          },
          loop: false,
      });
    }
    if(document.body.contains(swiper1)) {  
      mobile_only = new Swiper('.carousel .swiper-container', {            
          slidesPerView: 2,
          spaceBetween: 25,
          freeMode: true,
          draggable: true,
          breakpoints: {
            992: {
              slidesPerView: 1
            }
          },
          scrollbar: {
            el: '.swiper-scrollbar',
            draggable: true
          },
          loop: false,
      });
    }
    if(document.body.contains(swiper2)) {  
      var masters = new Swiper('.masters-carousel.swiper-container', {            
        slidesPerView: 3,
        spaceBetween: 20,
        draggable: true,
        breakpoints: {
          768: {
            slidesPerView: 2
          },
          560: {
            slidesPerView: 1
          }
        },
        loop: false,
    });
  }
// } else if (!this.media.matches && this.mobile_only != undefined) {
//   this.mobile_only.destroy();
//   this.mobile_only = undefined;
//   this.container.removeAttribute("style");     
// } 
}