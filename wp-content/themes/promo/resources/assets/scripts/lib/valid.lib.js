import hasClass from 'dom-helpers/class/hasClass';
import removeClass from 'dom-helpers/class/removeClass';
import {createEvent, find} from "./eventbus.lib";


export function valid_init() {
  var host = find('.wpcf7-form-control-wrap', document);
  var input = find('.wpcf7-form-control', host);
  createEvent(input, 'focus', allclear.bind(this));
}

function allclear({target}) {
  if(hasClass(target, 'wpcf7-not-valid')) {
    var host = target.closest('.wpcf7-form-control-wrap');
    var alert = host.querySelector('span[role="alert"]');
    host.removeChild(alert);
    removeClass(target, 'wpcf7-not-valid');
    
  }
}
// return '.wpcf7-form-control-wrap';
//   this.input = this.find('.wpcf7-form-control');
//   this.createEvent(this.input, 'focus', this.allclear.bind(this));
// }
// allclear() {
//   if(hasClass(this.input, 'wpcf7-not-valid')) {
//       alert = this.find('span[role="alert"]');
//       removeClass(this.input, 'wpcf7-not-valid');
//       this.host.removeChild(alert);
//   }
// }