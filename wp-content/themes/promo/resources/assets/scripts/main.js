
import {lozad_init} from "./lib/lazyload.lib";
import {cf7_init} from "./lib/cf7.lib";
import {modal_init} from "./lib/modal.lib";
import {mask_init} from "./lib/mask.lib";
import {form_init} from "./lib/form.lib";
import {notification_init} from "./lib/notification.lib";
// import {order_modal} from "./lib/order_modal.lib"; //something goes wrong
import {valid_init} from "./lib/valid.lib";
import {nav_init} from "./lib/nav.lib";

lozad_init();
form_init();
mask_init();
modal_init();
valid_init();
// order_modal();
notification_init();
cf7_init();
nav_init();