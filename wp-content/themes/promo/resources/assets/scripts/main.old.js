// Import everything from autoload
import App from './app/app'; // Base application class
import bootstrap from './app/bootstrap'; // Bootstrap utility
import windowLoad from './app/windowLoad'; // Window load hook
import './app/polyfills';
import 'bootstrap';
bootstrap(App);
windowLoad();