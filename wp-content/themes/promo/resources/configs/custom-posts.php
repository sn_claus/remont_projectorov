<?php
if ( ! function_exists( 'register_post_type_spareparts' ) ) :
	function register_post_type_spareparts() {
		$spareparts_labels = array(
			'name' => 'Услуги',
			'singular_name' => 'Услуги',
			'add_new' => 'Добавить новую',
			'add_new_item' => 'Добавить услугу',
			'edit_item' => 'Редактировать услугу',
			'new_item' => 'Новая услуга',
			'search_items' => 'Искать услугу',
			'not_found' =>  'Услуга не найдены.',
			'not_found_in_trash' => 'В корзине нет услуги.',
			'all_items' => 'Все услуги',
			'archives' => 'Архив услуг',
			'menu_name' => 'Услуги',
			'filter_items_list' => 'Фильтровать услуги',
			'items_list_navigation' => 'Навигация по услугам',
			'items_list' => 'Перечень услуг',
			'name_admin_bar' => 'Услуги'
		);
		$spareparts_args = array(
			'rewrite' => true,
			'labels' => $spareparts_labels,
			'public' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-media-spreadsheet',
			'hierarchical' => true,
			'supports' => array('title','editor', 'thumbnail', 'page-attributes'),
			'has_archive' => true,
			'show_in_nav_menus' => false,
			'show_ui' => true,
		);
		register_post_type('uslugi', $spareparts_args);
	}
	add_action( 'init', 'register_post_type_spareparts' );
endif;


if ( ! function_exists( 'spart_unregister_uslugi_columns' ) ) :
	function spart_unregister_uslugi_columns( $columns ) {
		unset($columns['title']);
		unset($columns['date']);
		return $columns;
	}
	add_filter( 'manage_edit-uslugi_columns', 'spart_unregister_uslugi_columns' );
endif;

if ( ! function_exists( 'register_uslugi_columns' ) ) :
	function register_uslugi_columns( $columns ) {
		$columns['title'] = 'Заголовок';
		$columns['cena'] = 'Цена за слугу';
		$columns['brands'] = 'Бренды';
		$columns['date'] = 'Дата';
		return $columns;
	}
	add_filter( 'manage_edit-uslugi_columns', 'register_uslugi_columns' );
endif;

if ( ! function_exists( 'fill_uslugi_columns' ) ) :
	function fill_uslugi_columns( $column ) {
		global $post;

		$cena = get_field("cf1", $post->ID);
		$parent = $post->post_parent;

		switch ( $column ) {
			case 'cena':
			if($parent){
				$zn = '';
			} else {
				$zn = '~';
			}
			if($cena) {
				echo '<div class="cena">'.$zn.' '. $cena . ' р.</div>';
			} else {
				echo '<span class="no-cena"> --- </span>';
			}     
			break;
			case 'brands':
			$cur_terms = get_the_terms( $post->ID, 'brands' );
			if( is_array( $cur_terms ) ){
				foreach( $cur_terms as $key => $cur_term ){
					if($key ==! 0) { echo ', ';}
					echo '<a href="'. get_edit_term_link( $cur_term->term_id, $cur_term->taxonomy ) .'">'. $cur_term->name .'</a>';
				}
			}
			else {
				echo 'Нет выбранных брендов.';
			}
			break;   
		}
	}
	add_action( 'manage_uslugi_posts_custom_column', 'fill_uslugi_columns', 10, 2 );
endif;

add_action( 'init', 'create_tag_taxonomies', 0 );

function create_tag_taxonomies() 
{
  $labels = array(
    'name' => __( 'Бренды' ),
    'singular_name' => __( 'бренд' ),
    'search_items' =>  __( 'Поиск брендов' ),
	'popular_items' => __( 'Популярные бренды' ),
	'not_found'		=> __('Брендов не найдено'),
    'all_items' => __( 'все бренды' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Редактировать бренд' ), 
    'update_item' => __( 'Сохранить' ),
    'add_new_item' => __( 'Добавить новый бренд' ),
    'new_item_name' => __( 'Новое имя бренда' ),
    'separate_items_with_commas' => __( 'разделять бренды запятой' ),
    'add_or_remove_items' => __( 'Добавить или удалить бренд' ),
    'choose_from_most_used' => __( 'Выбрать из частоиспользуемых брендов' ),
    'menu_name' => __( 'Бренды' ),
  ); 

  register_taxonomy('brands','uslugi',array(
    'hierarchical' => true,
    'labels' => $labels,
	'show_ui' => true,
	'_builtin' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'hierarchical' => true, 'slug' => 'brands' ),
  ));
}