<?php
// Чистка Head'а                                
/*******************************************************************************
Убираем излишние meta теги системы wordpress
********************************************************************************/
function wpcl_remove_meta_tags(){
  remove_action('wp_head', 'wp_generator');//выводит номер версии движка.
  remove_action('wp_head', 'wlwmanifest_link');//используется блог-клиентом Windows Live Writer.
  remove_action('wp_head', 'start_post_rel_link', 10, 0);
  remove_action('wp_head', 'parent_post_rel_link', 10, 0);
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);//Ссылки на соседние статьи (<link rel='next'... <link rel='prev'...)
  remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);//Короткая ссылка, ссылка без ЧПУ <link rel='shortlink' 
}
$wpcl_remove_meta_tags = 'wpcl_remove_meta_tags';
$wpcl_remove_meta_tags();

/*******************************************************************************
Удаление <link rel='dns-prefetch'
********************************************************************************/
//пока пользователь находится на странице и мощности компьютера простаивают вхолостую, можно самостоятельно указать для загрузки либо статическое изображение, либо js-библиотеку, либо целую страницу, которые теоретически понадобятся этому пользователю для дальнейшей работы с сайтом.

function wpcl_remove_dns_prefetch(){
	remove_action( 'wp_head', 'wp_resource_hints', 2 );
}
$wpcl_remove_dns_prefetch = 'wpcl_remove_dns_prefetch';
$wpcl_remove_dns_prefetch();

/*******************************************************************************
Удаление RSS-ленты
********************************************************************************/
function wpcl_disable_feed(){
	wp_redirect(get_option('siteurl'));
}

add_action('do_feed', 'wpcl_disable_feed', 1);
add_action('do_feed_rdf', 'wpcl_disable_feed', 1);
add_action('do_feed_rss', 'wpcl_disable_feed', 1);
add_action('do_feed_rss2', 'wpcl_disable_feed', 1);
add_action('do_feed_atom', 'wpcl_disable_feed', 1);

remove_action( 'wp_head', 'feed_links_extra', 3 );//выводит ссылки на дополнительные RSS-ленты сайта.
remove_action( 'wp_head', 'feed_links', 2 );//выводит ссылки на основные RSS-ленты сайта. 
remove_action( 'wp_head', 'rsd_link' );//используется блог-клиентами.


/*******************************************************************************
Защита - убирает записи в админ панели что введенный пароль/логин не верный
********************************************************************************/
function wpcl_remove_login_text(){
	add_filter('login_errors',create_function('$a', "return null;"));
}
$wpcl_remove_login_text = 'wpcl_remove_meta_tags';
$wpcl_remove_login_text();

/*******************************************************************************
Удаляем Wp-json, Oembed, Embed
********************************************************************************/
function wpcl_remove_json_oe_em(){
  // Filters for WP-API version 1.x
	add_filter('json_enabled', '__return_false');
	add_filter('json_jsonp_enabled', '__return_false');
  // Отключаем сам REST API - Filters for WP-API version 2.x
	add_filter('rest_enabled', '__return_false');
	add_filter('rest_jsonp_enabled', '__return_false');

  // Отключаем фильтры REST API
	remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd');
	remove_action( 'wp_head', 'rest_output_link_wp_head', 10, 0);
	remove_action( 'template_redirect', 'rest_output_link_header', 11, 0);
	remove_action( 'auth_cookie_malformed', 'rest_cookie_collect_status');
	remove_action( 'auth_cookie_expired', 'rest_cookie_collect_status');
	remove_action( 'auth_cookie_bad_username', 'rest_cookie_collect_status');
	remove_action( 'auth_cookie_bad_hash', 'rest_cookie_collect_status');
	remove_action( 'auth_cookie_valid', 'rest_cookie_collect_status');
	remove_filter( 'rest_authentication_errors', 'rest_cookie_check_errors', 100);

  // Отключаем события REST API
	remove_action( 'init', 'rest_api_init');
	remove_action( 'rest_api_init', 'rest_api_default_filters', 10, 1);
	remove_action( 'parse_request', 'rest_api_loaded');

  // Отключаем Embeds связанные с REST API
	remove_action( 'rest_api_init', 'wp_oembed_register_route');
	remove_filter( 'rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10, 4);
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );  
}
// $wpcl_remove_json_oe_em = 'wpcl_remove_json_oe_em';
// $wpcl_remove_json_oe_em();

/*******************************************************************************
Отключаем Emoji WordPress (смайлы Эмодзи)
********************************************************************************/
function wpcl_disable_emoji(){
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' ); 
}
$wpcl_disable_emoji = 'wpcl_disable_emoji';
$wpcl_disable_emoji();

/*******************************************************************************
Отключаем функцию трекбек на себя
********************************************************************************/
function wpcl_disable_trackback_self(&$links){
	$site_url = get_option( 'home' );
	foreach ( $links as $key => $val)
		if(strpos( $val, $site_url ) !== false)unset($links[$key]);
}
add_action('pre_ping','wpcl_disable_trackback_self');

// Удалить админ-панель
add_filter('show_admin_bar', '__return_false');

// Удаление неиспользуемых столбцов списка записей 'page'
if ( ! function_exists( 'unregister_page_columns' ) ) :
	function unregister_page_columns( $columns ) {
		unset($columns['comments']);
		unset($columns['author']);
		unset($columns['date']);
		return $columns;
	}
	add_filter( 'manage_edit-page_columns', 'unregister_page_columns' );
endif;

// Сохранять файлы в этой папке
add_filter( 'pre_option_upload_path', function( $upload_path ) {
	return 'wp-content/themes/promo/uploads';
});

// Не создавать миниатюры для загружаемых изображений
if ( ! function_exists( 'disable_thumbnails_generator' ) ) :
	function disable_thumbnails_generator( $sizes ) {
		unset( $sizes['thumbnail'] );
		unset( $sizes['medium'] );
		unset( $sizes['medium_large'] );
		unset( $sizes['large'] );
		return $sizes;
	}
	add_filter( 'intermediate_image_sizes_advanced', 'disable_thumbnails_generator' );
endif;

// Настройка стандартных виджетов
if ( ! function_exists( 'custom_admin_widgets' ) ) :
	function custom_admin_widgets() {
		unregister_widget('WP_Widget_Archives');
		unregister_widget('WP_Widget_Calendar');
    	//unregister_widget('WP_Widget_Categories');
		unregister_widget('WP_Widget_Meta');
    	//unregister_widget('WP_Widget_Pages');
		unregister_widget('WP_Widget_Recent_Comments');
   	 	//unregister_widget('WP_Widget_Recent_Posts');
		unregister_widget('WP_Widget_RSS');
		unregister_widget('WP_Widget_Search');
		unregister_widget('WP_Widget_Tag_Cloud');
    	//unregister_widget('WP_Widget_Text');
    	//unregister_widget('WP_Nav_Menu_Widget');
	}
	add_action( 'widgets_init', 'custom_admin_widgets', 20 );
endif;


// Настройка виджетов страницы Консоли
if ( ! function_exists( 'custom_dashboard_widgets' ) ) :
	function custom_dashboard_widgets () {
    	//remove_meta_box('dashboard_quick_press','dashboard','side');
   	 	//remove_meta_box('dashboard_recent_drafts','dashboard','side');
		remove_meta_box('dashboard_primary','dashboard','side');
    	//remove_meta_box('dashboard_secondary','dashboard','side');
    	//remove_meta_box('dashboard_incoming_links','dashboard','normal');
    	//remove_meta_box('dashboard_plugins','dashboard','normal');
		remove_meta_box('dashboard_right_now','dashboard', 'normal');
    	//remove_meta_box('dashboard_recent_comments','dashboard','normal');
    	remove_meta_box('dashboard_activity','dashboard', 'normal');
    	remove_action('welcome_panel','wp_welcome_panel');
	}
	add_action('wp_dashboard_setup', 'custom_dashboard_widgets');
endif;


// Настройка панели администратора
if ( ! function_exists( 'custom_admin_bar_menu' ) ) :
	add_action( 'admin_bar_menu', 'custom_admin_bar_menu', 999 );
	function custom_admin_bar_menu( $wp_admin_bar ) {
		$wp_admin_bar->remove_node('wp-logo');
    	//$wp_admin_bar->remove_node('dashboard');
    	//$wp_admin_bar->remove_node('about');
    	//$wp_admin_bar->remove_node('wporg');
    	//$wp_admin_bar->remove_node('documentation');
    	//$wp_admin_bar->remove_node('support-forums');
    	//$wp_admin_bar->remove_node('feedback');
    	//$wp_admin_bar->remove_node('site-name');
		$wp_admin_bar->remove_node('view-site');
		$wp_admin_bar->remove_node('dashboard');
		$wp_admin_bar->remove_node('themes');
		$wp_admin_bar->remove_node('widgets');
		$wp_admin_bar->remove_node('menus');
		$wp_admin_bar->remove_node('background');
		$wp_admin_bar->remove_node('header');
    	//$wp_admin_bar->remove_node('updates');
		//$wp_admin_bar->remove_node('customize');
		$wp_admin_bar->remove_node('comments');
    	//$wp_admin_bar->remove_node('new-content');
		$wp_admin_bar->remove_node('new-post');
		$wp_admin_bar->remove_node('new-media');
    	//$wp_admin_bar->remove_node('new-page');
		$wp_admin_bar->remove_node('new-user');
    	//$wp_admin_bar->remove_node('my-account');
	}
endif;

// Настройка меню администратора
if ( ! function_exists( 'custom_admin_menu' ) ) :
	add_action( 'admin_menu', 'custom_admin_menu', 999 );
	function custom_admin_menu(){
    	//remove_menu_page( 'index.php' );
		//remove_menu_page( 'edit.php' );
    	//remove_menu_page( 'upload.php' );
    	//remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'edit-comments.php' );
    	//remove_menu_page( 'themes.php' );
    	//remove_menu_page( 'plugins.php' );
		//remove_menu_page( 'users.php' );
		remove_menu_page( 'tools.php' );
    	//remove_menu_page( 'options-general.php' );
	}
endif;

// Удаление не нужных панелей в customize

function customize_register() {     
	global $wp_customize;
	$wp_customize->remove_section( 'custom_css' );
	$wp_customize->remove_panel( 'nav_menus' );
	$wp_customize->remove_section( 'title_tagline' );
	$wp_customize->remove_section( 'static_front_page' );
	$wp_customize->remove_panel( 'themes' );
} 

add_action( 'customize_register', 'customize_register', 11 );

if(!function_exists('remove_css_js_ver')):
	function remove_css_js_ver( $src ) {
		if( strpos( $src, '?ver=' ) )
			$src = remove_query_arg( 'ver', $src );
		return $src;
	}
	add_filter( 'style_loader_src', 	'remove_css_js_ver', 10, 2 );
	add_filter( 'script_loader_src', 	'remove_css_js_ver', 10, 2 );
endif;

// Отключение фильтра по датам
if ( ! function_exists( 'spart_disable_months_dropdown' ) ) :
  function spart_disable_months_dropdown( $columns ) {
    return true;
  }
  add_filter( 'disable_months_dropdown' , 'spart_disable_months_dropdown' );
endif;

add_action( 'wpcf7_init', 'custom_add_form_tag_siteurl' );
 
function custom_add_form_tag_siteurl() {
    wpcf7_add_form_tag( 'siteurl', 'custom_siteurl_form_tag_handler' ); // "clock" is the type of the form-tag
}
 
function custom_siteurl_form_tag_handler( $tag ) {
    return get_site_url();
}
if( 'disable_gutenberg' ){
	add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );

	// отключим подключение базовых css стилей для блоков
	// ВАЖНО! когда выйдут виджеты на блоках или что-то еще, эту строку нужно будет комментировать
	remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );

	// Move the Privacy Policy help notice back under the title field.
	add_action( 'admin_init', function(){
		remove_action( 'admin_notices', [ 'WP_Privacy_Policy_Content', 'notice' ] );
		add_action( 'edit_form_after_title', [ 'WP_Privacy_Policy_Content', 'notice' ] );
	} );
}
function ikreativ_async_scripts($url)
{
    if ( strpos( $url, '_asyncload_') === false )
        return $url;
    else if ( is_admin() )
        return str_replace( '_asyncload_', '', $url );
    else
	return str_replace( '_asyncload_', '', $url )."' async='async"; 
    }
add_filter( 'clean_url', 'ikreativ_async_scripts', 11, 1 );
