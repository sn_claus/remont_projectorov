<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);
class web_walker extends Walker_Nav_Menu {

		private $show_caret;
		private $target_device; //мобильное меню ('mobile')?

		function __construct($show_caret = true, $target_device=false ) { // в конструкторе
			//$this->open_submenu_on_hover = $open_submenu_on_hover; // запишем параметр раскрывания субменю
			$this->show_caret = $show_caret;
			$this->target_device = $target_device;
	    }

		function start_lvl(&$output, $depth = 0, $args = array()) { // старт вывода подменюшек
			$output .= "\n<ul class=\"nav-dropdown\">\n"; // ул с классом
		}
		function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) { // старт вывода элементов
			$item_html = ''; // то что будет добавлять
			parent::start_el($item_html, $item, $depth, $args); // вызываем стандартный метод родителя
			if ( $item->is_dropdown && $depth === 0 ) { // если элемент содержит подменю и это элемент первого уровня
			   if ($this->target_device === 'mobile') $item_html = str_replace('<a', '<button class="dropdown_toggle mobile fa fa-caret-down" role="button"></button> <a', $item_html); // если подменю не будет раскрывать при наведении надо добавить стандартные атрибуты бутстрапа для раскрытия по клику
			   else $item_html = str_replace('</a>', ' <b class="caret"></b></a>', $item_html); // это стрелочка вниз
            }

             if($item->current) {
                $item_html = str_replace($item->title, '<span class="cur-text">'.$item->title.'</span>', $item_html);
                $item_html = str_replace('<a href="', '<span class="cur" data-url="', $item_html);
                $item_html = str_replace('</a>', '</span>', $item_html);
            }
			$output .= $item_html; // приклеиваем теперь
		}
		function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) { // вывод элемента
			if ( $element->current ) $element->classes[] = 'active'; // если элемент активный надо добавить бутстрап класс для подсветки
			$element->is_dropdown = !empty( $children_elements[$element->ID] ); // если у элемента подменю
			if ( $element->is_dropdown ) { // если да
			    if ( $depth === 0 ) { // если li содержит субменю 1 уровня
			        $element->classes[] = 'dropdown'; // то добавим этот класс
			        //if ($this->open_submenu_on_hover) $element->classes[] = 'show-on-hover'; // если нужно показывать субменю по хуверу
			    } elseif ( $depth === 1 ) { // если li содержит субменю 2 уровня
			        $element->classes[] = 'dropdown-submenu'; // то добавим этот класс, стандартный бутстрап не поддерживает подменю больше 2 уровня по этому эту ситуацию надо будет разрешать отдельно
			    }
			}
			parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output); // вызываем стандартный метод родителя
		}
    }
    function get_excerpt($limit, $source = null) {

        if ($source == "post_content" ? ($excerpt = get_the_content()) : ($excerpt = $source))
            $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
        $excerpt = strip_shortcodes($excerpt);
        $excerpt = strip_tags($excerpt);
        $excerpt = mb_substr($excerpt, 0, $limit);
    
        if ($limit === mb_strlen($excerpt)) {
            $excerpt = $excerpt . '...';
        }
        /* $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
          $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt)); */
    
        return $excerpt;
    }
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page(array(
            'page_title' 	=> 'Настройки темы',
            'menu_title' 	=> 'Настройки темы',
            'icon_url'      => 'dashicons-admin-tools',
            'menu_slug'     => 'acf-options-nastrojki-temy',
            'redirect' 	=> false           
            ));    
        acf_add_options_page(array(
            'page_title' 	=> 'Отзывы',
            'menu_title' 	=> 'Отзывы',
            'icon_url'      => 'dashicons-testimonial',
            'menu_slug' 	=> 'reviews',
            'capability' 	=> 'edit_posts',
            'redirect' 	=> false
        )); 
    }
    add_image_size( 'fav', 16, 16, array( 'center', 'center')  );
    add_filter('wp_get_attachment_image_attributes', function ($attr, $attachment) {
        // Bail on admin
        if (is_admin()) {
            return $attr;
        }
    
        $attr['data-src'] = $attr['src'];
        $attr['class'] .= ' lazyload';
        unset($attr['src']);
    
        return $attr;
    }, 10, 2);
    function my_query_vars( $qvars ){
		$qvars[] = 'set';
		return $qvars;
	}
	add_filter('query_vars', 'my_query_vars' );
    
    // function remove_menus()
    // {
    //     global $menu;
    //     global $current_user;
    //     get_currentuserinfo();
    
    //     if($current_user->user_login != 'claus')
    //     {
    //         $restricted = array(__('Appearance'),
    //                             __('Plugins'),
    //                             __('Users'),

    //         );
    //         end ($menu);
    //         while (prev($menu)){
    //             $value = explode(' ',$menu[key($menu)][0]);
    //             if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
    //         }// end while
    
    //     }// end if
    // }
    
    function remove_acf_menu(){
        $current_user = wp_get_current_user();
        if ($current_user->user_login!='claus'){
        //   remove_menu_page( 'edit.php?post_type=acf-field-group' );
        }
      }
      add_action( 'admin_menu', 'remove_acf_menu', 100 );

    require_once ('configs/functions.php' );
    require_once ('configs/custom-posts.php' );


    add_filter( 'body_class','my_body_classes' );
    function my_body_classes( $classes ) {
        if(get_field('text_size', 'options')) {
            $classes[] = get_field('text_size', 'options') ;
        }
        return $classes;
    }
    function text_area_shortcode($value, $post_id, $field) {
        if (is_admin()) {
          // don't do this in the admin
          // could have unintended side effects
      
          // revision: return $value because we don't want to miss on the textarea content
          return $value;
        }
      
        return do_shortcode($value);
      }
      add_filter('acf/load_value/type=textarea', 'text_area_shortcode', 10, 3);

// function make_mce_awesome( $init ) {
//     $init['plugins'] = 'lineheight'; 
//     // $init['toolbar'][] = 'lineheightselect';
//     $init['lineheight_formats'] = "1 1.3 1.5 1.7 2 2.5 2.7 3";
// 	return $init;
// }

// add_filter('tiny_mce_before_init', 'make_mce_awesome');

function sh_custom_add_tinymce_button_callback($buttons) {
    //Set the custom button identifier to the $buttons array
    $buttons[] = "lineheightselect";
    
    //return $buttons
    return $buttons;
}

// var_dump(get_query_var('pagename'));

add_filter('mce_buttons_2', 'sh_custom_add_tinymce_button_callback');
if(get_field('api_on', 'options')) {
    add_action( 'wpcf7_before_send_mail', 'my_conversion' );
    add_action( 'admin_post_venyoo', 'chatbot_conversion' );
    add_action( 'admin_post_nopriv_venyoo', 'chatbot_conversion');
}

   function my_conversion( $contact_form ) {
    $submission = WPCF7_Submission::get_instance();
    if ( $submission ) {
    $posted_data = $submission->get_posted_data();
    $remote_ip = $submission->get_meta( 'remote_ip' );
    $url = $submission->get_meta( 'url' );
    $phone = preg_replace('/(\W*)/', "", $posted_data["your-tel"]);
    // $phone = substr( $phone, 1);
    $host = get_field('host', 'options');
    $api_url = get_field('api', 'options');
    $token = get_field('token', 'options');
    $title = $posted_data["title-id"];
    // $title = get_the_title();
    if($title) {
        $data = "phone={$phone}&message={$title}&host={$host}&remote_ip={$remote_ip}";
    }
    else {
        $data = "phone={$phone}&host={$host}&remote_ip={$remote_ip}";
    }

        $ch = curl_init($api_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:Token '.$token));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,5); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 5); 
        $result = curl_exec($ch);
        curl_close($ch);
        

    if (!is_string($result) || !strlen($result)) {
        $msg = "Failure Contacting Server";
        $path = dirname(__FILE__) . '/test.txt';
        if (($h = fopen($path, "a")) !== FALSE) {
            $msg = print_r($error_message, true);
            $mystring = $u . '* ' . $msg . PHP_EOL;
            $mystring2 = $u . ' ' . $data . PHP_EOL;
            fwrite( $h, $mystring2 );
            fwrite( $h, $mystring );
            fclose($h);
        }
    } else {
        $msg = print_r($result, true);
        $path = dirname(__FILE__) . '/test.txt';
        if (($h = fopen($path, "a")) !== FALSE) {
            $mystring = $u . '* ' . $msg . PHP_EOL;
            $mystring2 = $u . ' ' . $data . PHP_EOL;
            fwrite( $h, $mystring2 );
            fwrite( $h, $mystring );
            fclose($h);
        }
    }
    return;
    }
  }
  
    
 

  function chatbot_conversion(){
    $remote_ip = $_POST['client_ip'];
    $phone = $_POST['phone'];
    $host = get_field('host', 'options');
    $api_url = get_field('api', 'options');
    $token = get_field('token', 'options');
    $title = 'Заявка через чатбота';
    // $title = get_the_title();
    if($title) {
        $data = "phone={$phone}&message={$title}&host={$host}&remote_ip={$remote_ip}";
    }
    else {
        $data = "phone={$phone}&host={$host}&remote_ip={$remote_ip}";
    }

        $ch = curl_init($api_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:Token '.$token));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,5); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 5); 
        $result = curl_exec($ch);
        curl_close($ch);

        if (!is_string($result) || !strlen($result)) {
            $msg = "Failure Contacting Server";
            $path = dirname(__FILE__) . '/test2.txt';
            if (($h = fopen($path, "a")) !== FALSE) {
                $msg = print_r($error_message, true);
                $mystring = $u . '* ' . $msg . PHP_EOL;
                $mystring2 = $u . ' ' . $data . PHP_EOL;
                fwrite( $h, $mystring2 );
                fwrite( $h, $mystring );
                fclose($h);
            }
        } else {
            $msg = print_r($result, true);
            $path = dirname(__FILE__) . '/test2.txt';
            if (($h = fopen($path, "a")) !== FALSE) {
                $mystring = $u . '* ' . $msg . PHP_EOL;
                $mystring2 = $u . ' ' . $data . PHP_EOL;
                fwrite( $h, $mystring2 );
                fwrite( $h, $mystring );
                fclose($h);
            }
        }
        return;
  }
  function dequeue_jquery_migrate( $scripts ) {
    if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) {
        $scripts->registered['jquery']->deps = array_diff(
            $scripts->registered['jquery']->deps,
            [ 'jquery-migrate' ]
        );
    }
}
add_action( 'wp_default_scripts', 'dequeue_jquery_migrate' );

add_filter( 'wpcf7_form_elements', 'do_shortcode' );

add_filter( 'wpseo_title', 'change_one_wpseo_title' );

function change_one_wpseo_title($title) {
    if(is_singular('uslugi')) {
        $title.=" от ".(get_field('cf1') ? get_field('cf1') : '000' ).' руб. '.(get_field('default_price2', 'options') ? 'c выездом по' : 'в' ).' Москве';
        // var_dump($title);
        return $title;
    } else {
        $title.=" от ".(get_field('default_price', 'options') ? get_field('default_price', 'options') : '400' ).' руб. '.(get_field('default_price2', 'options') ? 'c выездом по' : 'в' ).' Москве';
        // var_dump($title);
        return $title;
    }
}
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5de8ef1903dee',
        'title' => 'SEO title',
        'fields' => array(
            array(
                'key' => 'field_5de8efecbf49d',
                'label' => 'Минимальная цена',
                'name' => 'default_price',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 400,
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5de8f003bf49e',
                'label' => 'с Выездом',
                'name' => 'default_price2',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-nastrojki-temy',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
    
    endif;