@extends('layouts.app')

@section('content')
 <div class="container h-100 d-flex flex-wrap position-absolute juctify-content-center">
    <div class="row w-100">
      <div class="col-12 d-flex flex-wrap alig-items-center flex-column justify-content-center text-center">
        <div class="alert alert-warning">
          <h1>{{ __('К сожалению, мы не смогли найти страницу, которую вы запрашиваете.', 'sage') }}</h1>
          {{-- {{get_search_form()}} --}}
        </div>
      </div>
    </div>
  </div>
@endsection
