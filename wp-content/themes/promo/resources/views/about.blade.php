{{-- 
    Template name: about
--}}
@extends('layouts.app')

@section('content')
@while(have_posts()) @php the_post() @endphp
<div class="container">
  <div class="row">
      <div class="page-bg" style="
        background-image: url({!! ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url'])!!}
        )"></div>
    </div>
    <div class="row">
        <div class="col-12 col-title">
            <h1 id="p_title" class="tri-title page-title triafter tri-medium">{!!get_field('p_title') ? get_field('p_title') : get_the_title()!!}</h1>            
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xl-12">
            <div class="row">
                <div class="col-12 col-content {!!(get_field('add_content') && get_field("дополонительная_колонка") ? 'col-xl-6' : '')!!}">
                    {!!get_the_content()!!}
                    <div class="form-init d-xxl-none col-12 form-id horizontal-form big">
                        {!!do_shortcode('[contact-form-7 id="137" title="Order with ID"]');!!}
                    </div>
                    <div class="col-masters">
                        <h3>Лучшие мастера</h3>
                        <div class="swiper-container masters-carousel">
                            <div class="swiper-wrapper">
                                @foreach (get_field('masters') as $item)
                                    <div class="swiper-slide">
                                        <div class="img-wrap">
                                            <img alt="{!! $item['img']['alt']!!}" class="img-fluid lazyload" title="{{$item['img']['title']}}" src="#" data-src="{{$item['img']['url']}}">
                                            <div class="swiper-lazy-preloader"></div>
                                        </div>
                                        <div class="item-title">{!!$item['full']['name']!!}</div>
                                        <div class="item-body">{!!$item['full']['desc']!!}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-addcontent">
                            {!! get_field('content_about')!!}
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-6 col-addcontent d-none d-xxl-block">
                    @if (get_field("add_content"))
                        {!!get_field("add_content")!!}
                    @endif
                    <div class="form-init d-none d-xl-block col-12 form-id horizontal-form">
                        {!!do_shortcode('[contact-form-7 id="137" title="Order with ID"]');!!}
                    </div>
                </div>
              
            </div>
        </div>
    </div>
</div>
@endwhile
@endsection
