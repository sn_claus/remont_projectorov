{{-- 
    Template name: brands 
--}}
@extends('layouts.app')
@php
$terms = get_terms([
    'taxonomy' => 'brands',
    'hide_empty' => false,
    'parent' => 0,
]);
@endphp
@section('content')
@while(have_posts()) @php the_post() @endphp
    <div class="container">
        <div class="row">
            <div class="page-bg d-none d-lg-block" style="background-image: url({!! ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url'] )!!})">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-title">
                <h1 id="p_title" class="tri-title page-title triafter tri-medium">{!!get_field('p_title') ? get_field('p_title') : get_the_title()!!}</h1>            
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 col-content">
                        
                        @foreach ($terms as $item)
                            <a class="item-brand" href="{!!get_term_link($item->term_id)!!}">
                                <div class="item-img">
                                    <img alt="{!! get_field('img', 'brands_'.$item->term_id)['alt']!!}" class="img-fluid lazyload" title="{{get_field('img', 'brands_'.$item->term_id)['title']}}" src="#" data-src="{{get_field('img', 'brands_'.$item->term_id)['url']}}">
                                    <div class="swiper-lazy-preloader"></div>
                                </div>
                                <div class="item-content">
                                    <p>{!! term_description($item->term_id)!!}</p>
                                </div>
                            </a>
                        @endforeach
                    </div>
                    @if (get_field("add_content"))
                        <div class="col-12 col-xl-6 col-addcontent">
                            {!!get_field("add_content")!!}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-12 col-addcontent">
                        {!!get_the_content()!!}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endwhile
@endsection
