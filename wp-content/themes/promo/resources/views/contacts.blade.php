{{-- 
    Template name: Contacts
--}}
@extends('layouts.app')

@section('content')
@while(have_posts()) @php the_post() @endphp
    <div class="container fix-bg">
        <div class="row d-none d-xl-flex">
            @if(get_field("hide_map", 'option'))
                <div class="page-bg lazyload" data-background-image="{!! ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url'] )!!}">
                </div>
            @else
                <div class="page-bg">
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-12 col-xl-6 col-content">
                <div class="row">
                    <div class="col-12 col-title">
                        <h1 id="p_title" class="tri-title page-title triafter tri-medium">{!!get_field('p_title') ? get_field('p_title') : get_the_title()!!}</h1>            
                    </div>
                    <div class="col-12 col-content">
                        {!!get_the_content()!!}
                    </div>
                    <div class="col-sm-6 col-12 col-addcontent">
                        <span class="blue-text">тел: <a class="white-link" onclick="{!!$phone_click!!}" href="tel:{{preg_replace('![^0-9]+!', '', get_field('tel', 'options'))}}">{!!get_field('tel', 'options')!!}</a></span>
                        <p class="blue-text">{!!get_field('time_work', 'options')!!}</p>
                    </div>
                    <div class="col-sm-6 col-12 col-addcontent">
                        @if(get_field('office', 'options'))
                            <span class="blue-text">Центральный офис:</span>
                            <p>{!!get_field('office', 'options')!!}</p>
                        @endif
                    </div>
                </div>
            </div>
            @if (get_field("add_content"))
                @if(!get_field("hide_map", 'option'))
                    <div class="col-12 col-xl-6 col-addcontent map">
                        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                        <script>
                            var YaMapsWP = {}, YMlisteners = {};
                        </script>
                        {!!get_field("map", "options")!!}
                    </div>
                @endif
            @endif
        </div>
    </div>
@endwhile
@endsection
