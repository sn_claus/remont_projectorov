{{--
  Template Name: Главная
--}}
@extends('layouts.app')

@section('content')
  @while (have_posts()) @php the_post() @endphp
    @include('frontpage.hero')
    @include('frontpage.trouble')
    @include('frontpage.order')
    @include('frontpage.benefits')
    @include('frontpage.promotion')
  @endwhile
@endsection
