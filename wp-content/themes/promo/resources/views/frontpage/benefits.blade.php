@php
    $query_args2 = array(
    'post_type' => 'post',
    'posts_per_page' => -1,
    'category' => 'benefits',
    'orderby' => 'date',
    'order' => 'ASC'
);
$query = new WP_Query( $query_args2 );
$uposts = $query->posts;
$counter = 0;
$bg= get_template_directory_uri().'/assets/images/benbg.png';
@endphp


<section class="benefits">
    <div class="container">
        <div class="row ben-row">
            @foreach($uposts as $post)
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="item-wrapper triafter">
                        <div class="item-title">                       
                            <h2 class="tri-title multi">{!!(get_field('p_title', $post->ID) ? get_field('p_title', $post->ID) : $post->post_title)!!}</h2>
                        </div>
                        <div class="item-body linebefore">
                            {!!$post->post_content!!}
                        </div>
                        <div class="item-actions">
                            <a href="{!!(get_field('parent', $post->ID)? get_permalink(get_field('parent', $post->ID)) : get_permalink($post->ID))
                                !!}" class="btn default">
                                @icon(['name' => 'arrow'])
                                @endicon <span>Подробнее</span>
                            </a>
                        </div>
                        <div class="item-bg">
                            <img alt="item-bg" class="img-fluid lazyload" src="#" data-src="{!! ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : $bg )!!}">
                            <div class="swiper-lazy-preloader"></div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>