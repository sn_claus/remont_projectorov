<section class="hero">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <h1 class="light-title section-title">{!!get_field('p_title')!!}</h1>
                <div class="hero-desc">
                    {!!get_the_content()!!}
                </div>
                <button data-micromodal-trigger="order" class="btn light d-none d-xl-inline-block">
                    @if(get_field('buttons', 'options'))
                        {!!get_field('buttons', 'options')!!}
                    @else
                    Заказать ремонт 
                    @endif
                    @icon(['name' => 'arrow'])
                    @endicon
                </button>
                <ul class="social-hero d-none d-xl-flex">
                    @if(get_field('a_soc', 'options'))
                        @if(in_array("vk", get_field('a_soc', 'options')))
                            <li>
                                <a href="{!!get_field('vk', 'options')!!}" target="_blank">
                                    @icon(['name' => 'vk'])
                                    @endicon
                                </a>
                            </li>
                        @endif
                        @if(in_array("facebook", get_field('a_soc', 'options')))
                        <li>
                            <a href="{!!get_field('facebook', 'options')!!}" target="_blank">
                                @icon(['name' => 'facebook'])
                                @endicon
                            </a>
                        </li>
                        @endif
                        @if(in_array("Однокласники", get_field('a_soc', 'options')))
                        <li>
                            <a href="{!!get_field('odno', 'options')!!}" target="_blank">
                                @icon(['name' => 'odnoklassniki'])
                                @endicon
                            </a>
                        </li>
                        @endif
                        @if(in_array("twitter", get_field('a_soc', 'options')))
                        <li>
                            <a href="{!!get_field('twitter', 'options')!!}" target="_blank">
                                @icon(['name' => 'twitter'])
                                @endicon
                            </a>
                        </li>
                        @endif
                        @if(in_array("google-plus", get_field('a_soc', 'options')))
                        <li>
                            <a href="{!!get_field('google', 'options')!!}" target="_blank">
                                @icon(['name' => 'google-plus'])
                                @endicon
                            </a>
                        </li>
                        @endif
                    @endif
                </ul>
            </div>
            <div class="col-12 col-xl-6 tribefore d-flex flex-wrap xl-only fix-bg">
                <div class="row add-info order-2 order-xl-1 d-none d-sm-flex">
                    @if(get_field('hero_links', 'options'))
                        @foreach (get_field('hero_links', 'options') as $item)
                            <div class="col-auto">
                                <a class="line-before" href="{!!get_permalink($item['hero_link']->ID)!!}">{!!$item['text_link']!!}</a>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="row hero-img order-1 order-xl-2 d-none d-md-block {!! (get_field('hero_props', 'options') ? get_field('hero_props', 'options') : '')!!}">
                    <img class="logo-img lazyload" alt="{{get_field('hero', 'options')['alt']}}" src="#" data-src="{{get_field('hero', 'options')['url']}}">
                    <div class="swiper-lazy-preloader"></div>
                </div>
                <div class="row order-3 d-xl-none text-center w-100 justify-content-center my-5">
                    <button data-micromodal-trigger="order" class="btn light">Заказать ремонт 
                        @icon(['name' => 'arrow'])
                        @endicon
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>