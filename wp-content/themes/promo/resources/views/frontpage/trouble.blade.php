<section class="troubles {!! (get_field('hero_props', 'options') ? get_field('hero_props', 'options') : '')!!}" >
    <div class="container">
        <div class="row row-troubles">
            @php $counter = 0; @endphp
            @foreach (get_field('set') as $key => $item)
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper">
                    <div class="item-title w-100">
                        <a  href="{!!get_permalink($item->ID)!!}" class="tri-title triafter tri-small">{!!get_the_title($item->ID)!!}</a>
                    </div>
                    <div class="item-body w-100">
                        @if (get_field('others', $item->ID))
                            @foreach (get_field('others', $item->ID) as  $key => $service)
                                @if($key > 2) @break
                                @endif
                                <a href="{!!get_permalink($service->ID)!!}">{!!$service->post_title!!}</a>
                            @endforeach
                        @endif
                    </div>
                    <div class="item-actions w-100">
                        <a class="btn default" href="{!!get_permalink($item->ID)!!}">
                            @icon(['name' => 'arrow'])
                            @endicon <span>Подробнее</span>
                        </a>
                        <span class="item-price">от {!! (get_field('cf1', $item->ID) ? get_field('cf1', $item->ID) : '000'); !!} Р</span>
                    </div>
                    <div class="item-count">@php if($counter<9) {echo '0'.($counter + 1);} else {echo $counter + 1;}  $counter++; @endphp</div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>