@extends('layouts.app')

@section('content')
  @if (!have_posts())
    <div class="container h-100 d-flex flex-wrap position-absolute juctify-content-center">
      <div class="row w-100">
        <div class="col-12 d-flex flex-wrap alig-items-center flex-column justify-content-center text-center">
          <div class="alert alert-warning">
            <h2>{{ __('К сожалению, мы не смогли найти страницу, которую вы запрашиваете.', 'sage') }}</h2>
            {{-- {{get_search_form()}} --}}
          </div>
        </div>
      </div>
    </div>
   @elseif(get_post_type() == 'uslugi')
    <div class="container">
        <div class="row">
          <div class="page-bg lazyload" data-background-image="{!! ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url'] )!!}">
          </div>
        </div>
        <div class="row">
            <div class="col-12 col-title">
                <h1 class="tri-title page-title triafter tri-medium">{!! (get_field('p_title', 'brands_'.get_queried_object()->term_id) ? get_field('p_title', 'brands_'.get_queried_object()->term_id) : get_bloginfo( 'name' ).' <b>'.single_term_title( '', false ).'</b>') !!}</h1>
              </h1>
            </div>
            @if (get_field("add_content", 'brands_'.get_queried_object()->term_id))
                <div class="col-12 col-xl-6 col-content">
                    {!!get_field("add_content", 'brands_'.get_queried_object()->term_id)!!}
                </div>
            @endif
            
        </div>
    </div>
    <div class="carousel">
        <div class="container">
            <div class="row">
              <div class="form-init col-xl-6 col-12 form-id horizontal-form">
                {!!do_shortcode('[contact-form-7 id="89" title="Order"]');!!}
              </div>
            </div>
            <div class="row row-services swiper-container">
                <div class="swiper-wrapper">
                    <div class="col-12 col-lg-6 service-slide swiper-slide">
                        @php $counter = 0;  @endphp
                      
                        @if(get_field('custom', 'brands_'.get_queried_object()->term_id))
                          @foreach (get_field('custom_list', 'brands_'.get_queried_object()->term_id) as $item)
                            @php if($counter%5==0 && $counter!==0) echo '</div><div class="col-12 col-lg-6 service-slide swiper-slide">'; @endphp
                            <div class="item-wrapper">
                                <a href="{!!get_permalink($item->ID)!!}" class="item-title">
                                    {!!get_the_title($item->ID)!!}
                                </a>
                                <span class="item-price">от {!! (get_field('cf1', $item->ID) ? get_field('cf1', $item->ID) : '000'); !!} р</span>
                                <button data-micromodal-trigger="order" class="btn default v2">Заказать</button>
                            </div>
                            @php $counter++; @endphp
                          @endforeach
                        @else
                          @while (have_posts()) @php the_post() @endphp
                            @include('partials.content-'.get_post_type())
                            @php $counter++; @endphp
                          @endwhile
                        @endif
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="row row-models mt-5">
              <div class="col-12">
                @php
                  $cur = get_queried_object()->term_id;
                  $terms = get_terms([
                      'taxonomy' => 'brands',
                      'hide_empty' => false,
                      'child_of' => $cur,
                  ]);
                  if(!$terms) {
                    
                    $terms = get_terms([
                        'taxonomy' => 'brands',
                        'hide_empty' => false,
                        'parent' => get_queried_object()->parent,
                        'exclude' => get_queried_object()->term_id
                    ]);
                    if($terms) {
                      echo '<p class="mb-0">Другие модели: </p>';
                    }
                  }
                  else {
                    echo '<p class="mb-0">Известные модели: </p>';
                  }
                  @endphp
              
              </div>
              <div class="col-12">
                <div class="row w-100">
                  @if($terms)
                    @foreach ($terms as $item)
                      <div class="col-12 col-md-6 col-lg-4 col-xl-3 pr-md-4">
                        <a class="item-brand" href="{!!get_term_link($item->term_id)!!}">{!!$item->name!!}</a>
                      </div>
                    @endforeach
                  @endif
                </div>
              </div>
            </div>
        </div>
    </div>
    @else
    @include('partials.content-'.get_post_type())
    @endif
@endsection
