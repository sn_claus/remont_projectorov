@extends('layouts.app')

@section('content')
@while(have_posts()) @php the_post() @endphp
<div class="container">
  <div class="row">
        <div class="page-bg lazyload" data-background-image="{!! ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url'] )!!}">
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-title">
            <h1 id="p_title" class="tri-title page-title triafter tri-medium">{!!get_field('p_title') ? get_field('p_title') : get_the_title()!!}</h1>            
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xl-12">
            <div class="row">
              <div class="col-12 col-content {!!(get_field('add_content') && get_field("дополонительная_колонка") ? 'col-xl-6' : '')!!}">
                  {!!get_the_content()!!}
              </div>
              @if (get_field("add_content") && get_field("дополонительная_колонка"))
                <div class="col-12 col-xl-6 col-addcontent">
                    {!!get_field("add_content")!!}
                </div>
              @endif
            </div>
        </div>
    </div>
</div>
@endwhile
@endsection
