@php 
if($counter%5==0 && $counter!==0) echo '</div><div class="col-12 col-lg-6 service-slide swiper-slide">'; @endphp
<div class="item-wrapper">

    <a href="{!!get_permalink()!!}" class="item-title">
        {!!get_the_title()!!}
    </a>
    <span class="item-price">от {!! (get_field('cf1') ? get_field('cf1') : '000'); !!} р</span>
    <button data-micromodal-trigger="order" class="btn default v2">Заказать</button>
</div>
