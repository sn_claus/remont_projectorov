<footer class="floor">
  <div class="nav-footer w-100">
    <div class="container">
      <div class="floor-nav d-none d-xl-flex row">
        <nav class="main-nav @php echo (get_field('logo_only', 'options'))? 'img-only' : ''; @endphp">
          @if(!is_front_page())
            <a class="logo-wrap" href="{!!get_home_url()!!}">
                <img alt="{{get_field('logo', 'options')['alt']}}" class="logo-img lazyload" title="{{get_field('logo', 'options')['title']}}" src='#' data-src="{{get_field('logo', 'options')['url']}}">
                @if(!get_field('logo_only', 'options'))
                  <span class="logo-text">{!!get_field('logo_text', 'options')!!}</span>
                @endif
            </a>
          @else
          <div class="logo-wrap">
                <img  class="logo-img lazyload" alt="{{get_field('logo', 'options')['alt']}}" title="{{get_field('logo', 'options')['title']}}" src='#' data-src="{{get_field('logo', 'options')['url']}}">
                @if(!get_field('logo_only', 'options'))
                  <span class="logo-text">{!!get_field('logo_text', 'options')!!}</span>
                @endif
          </div>
          @endif
            <div class="nav-menu-toggler d-xl-none order-3">
              <button type="button" class="toggle-nav">
                <span></span>
                <span class="less"></span>
                <span class="less"></span>
                <span></span>
              </button>
              </div>
            <div class="nav nav-menu d-none d-xl-block">
              @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new web_walker]) !!}
              @endif
            </div>
            <button data-micromodal-trigger="order" class="btn light">
                @if(get_field('buttons', 'options'))
                        {!!get_field('buttons', 'options')!!}
                @else
                Заказать ремонт 
                @endif
                @icon(['name' => 'arrow'])
                @endicon
            </button>
          </nav>
      </div>
    </div>
  </div>
  <div class="main-floor w-100">
    <div class="container">
      <div class="row">
        <a class="phone-number" href="tel:{{preg_replace('![^0-9]+!', '', get_field('tel', 'options'))}}">{{get_field('tel', 'options')}}
        </a>
        <span class="copyright">{{get_field('copyright', 'options')}}</span>
        <div class="social-floor">
            <ul class="social-group d-flex">
                @if(get_field('a_soc', 'options'))
                  @if(in_array("vk", get_field('a_soc', 'options')))
                    <li>
                        <a href="{!!get_field('vk', 'options')!!}" target="_blank">
                            @icon(['name' => 'vk'])
                            @endicon
                        </a>
                    </li>
                  @endif
                  @if(in_array("facebook", get_field('a_soc', 'options')))
                  <li>
                      <a href="{!!get_field('facebook', 'options')!!}" target="_blank">
                          @icon(['name' => 'facebook'])
                          @endicon
                      </a>
                  </li>
                  @endif
                  @if(in_array("Однокласники", get_field('a_soc', 'options')))
                  <li>
                      <a href="{!!get_field('odno', 'options')!!}" target="_blank">
                          @icon(['name' => 'odnoklassniki'])
                          @endicon
                      </a>
                  </li>
                  @endif
                  @if(in_array("twitter", get_field('a_soc', 'options')))
                  <li>
                      <a href="{!!get_field('twitter', 'options')!!}" target="_blank">
                          @icon(['name' => 'twitter'])
                          @endicon
                      </a>
                  </li>
                  @endif
                  @if(in_array("google-plus", get_field('a_soc', 'options')))
                  <li>
                      <a href="{!!get_field('google', 'options')!!}" target="_blank">
                          @icon(['name' => 'google-plus'])
                          @endicon
                      </a>
                  </li>
                  @endif
                @endif
            </ul>
        </div>
    </div>
  </div>
  </div>
  <div class="nav-mobile">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'items_wrap' => '<ul id="%1$s" class="nav %2$s">%3$s</ul>', 'container'=> false,'walker' => new web_walker]) !!}
      @endif
    </div>
    <div class="other-stuff">
      <div class="micro_modal" id="order" aria-hidden="true">
          <div class="modal_overlay d-flex justify-content-center align-items-center" data-micromodal-close>
            <div class="modal_container triafter v2" role="dialog" aria-modal="true" aria-labelledby="order">
              <div class="modal-header d-flex justify-content-center align-items-center">
                <button class="modal-close toggle-nav opened" aria-label="Close modal" data-micromodal-close >
                    <span class="shape d-none"></span>
                    <span class="shape"></span>
                    <span class="shape"></span>
                    <span class="shape d-none"></span>
                </button>
                <span class="close-text">закрыть</span>
              </div>
                <div class="modal-content justify-content-center">
                  <h3 class="tri-title default">Заказ <b>ремонта</b></h3>
                  <p>Укажите Ваш номер телефона <br>и наш специалист перезвонит Вам в течении минуты. </p>
                  <div class="form-init">
                    {!! do_shortcode('[contact-form-7 id="89" title="Order"]'); !!}
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="micro_modal" id="order_id" aria-hidden="true">
            <div class="modal_overlay d-flex justify-content-center align-items-center" data-micromodal-close>
              <div class="modal_container triafter v2" role="dialog" aria-modal="true" aria-labelledby="order">
                <div class="modal-header d-flex justify-content-center align-items-center">
                  <button class="modal-close toggle-nav opened" aria-label="Close modal" data-micromodal-close >
                      <span class="shape d-none"></span>
                      <span class="shape"></span>
                      <span class="shape"></span>
                      <span class="shape d-none"></span>
                  </button>
                  <span class="close-text">закрыть</span>
                </div>
                  <div class="modal-content justify-content-center">
                    <h3 class="tri-title default">Заказ <b>ремонта</b></h3>
                    <p>Укажите Ваш номер телефона <br>и наш специалист перезвонит Вам в течении минуты. </p>
                    <div class="form-init">
                      {!! do_shortcode('[contact-form-7 id="143" title="Order with ID (modal)"]'); !!}
                    </div>
                  </div>
                </div>
              </div>
          </div>
        <div class="mdc-snackbar mdc-snackbar--leading">
            <div class="mdc-snackbar__surface">
              <div class="mdc-snackbar__label" role="status" aria-live="polite">
              </div>
            </div>
        </div>
        @if(!is_front_page())
        <div class="d-none">
          @include('frontpage.countdown')
        </div>
        @endif
        <div class="d-none">
        </div>
    </div>
</footer>