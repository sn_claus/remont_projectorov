<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  @if (get_field('keywords'))
    <meta name="keywords" content="{!!get_field('keywords')!!}" />
  @elseif(get_field('keywords', 'brands_'.get_queried_object()->term_id))
    <meta name="keywords" content="{!!get_field('keywords', 'brands_'.get_queried_object()->term_id)!!}" />
  @endif
  @php wp_head() @endphp
  <link rel="shortcut icon" href="{!!get_field('favicon', 'options')['sizes']['fav']!!}" type="image/png">
  <style>
    html {
      --primary-color1: {!!get_field('color1', 'options')!!};
      --primary-color2: {!!get_field('color2', 'options')!!};
      --primary-color3: {!!get_field('color3', 'options')!!};
      --secondary-color1: {!!get_field('color4', 'options')!!};
      --text-color1: {!!get_field('text_color', 'options')!!};
      --text-color2: {!!get_field('text_color2', 'options')!!};
    }
    section.promotion::before,section.promotion::after {
      background-image: url("{!!get_field('promotion', 'options')['url']!!}");
    }
</style>
</head>
