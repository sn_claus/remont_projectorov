<header>
  <div class="container">
    <nav class="main-nav @php echo (get_field('logo_only', 'options'))? 'img-only' : ''; @endphp">
      @if(!is_front_page())
        <a class="logo-wrap" href="{!!get_home_url()!!}">
            <img alt="{{(get_field('logo', 'options')['alt'] ? get_field('logo', 'options')['alt'] : get_field('logo', 'options')['title'])}}" class="logo-img" src="{{get_field('logo', 'options')['url']}}">
            @if(!get_field('logo_only', 'options'))
              <span class="logo-text">{!!get_field('logo_text', 'options')!!}</span>
            @endif
        </a>
      @else
      <div class="logo-wrap">
            <img  class="logo-img" alt="{{(get_field('logo', 'options')['alt'] ? get_field('logo', 'options')['alt'] : get_field('logo', 'options')['title'])}}"  src="{{get_field('logo', 'options')['url']}}">
            @if(!get_field('logo_only', 'options'))
              <span class="logo-text">{!!get_field('logo_text', 'options')!!}</span>
            @endif
      </div>
      @endif
      <div class="nav-menu-toggler d-xl-none order-3">
        <button type="button" class="toggle-nav">
          <span></span>
          <span class="less"></span>
          <span class="less"></span>
          <span></span>
        </button>
        </div>
      <div class="nav nav-menu d-none d-xl-block">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new web_walker]) !!}
        @endif
      </div>
      <div class="nav-call order-2 order-xl-3 ml-auto ml-xl-0">
          <a class="phone-number" href="tel:{{preg_replace('![^0-9]+!', '', get_field('tel', 'options'))}}">{{get_field('tel', 'options')}}
            <img alt="call" class="call-img" src="<?= get_template_directory_uri(); ?>/assets/images/call.png">
          </a>
          <span class="phone-city d-block">{{get_field('city', 'options')}}</span>
      </div>
    </nav>
  </div>
</header>