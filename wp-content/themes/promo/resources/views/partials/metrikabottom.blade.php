@php 
if (get_field('metrika', 'options')) {
    foreach (get_field('metrika', 'options') as $key => $value) {
        if($value['place'] === 'footer') {
            echo $value['code'];
        }
    }
}
@endphp
