{{--
  Template Name: reviews
--}}
@extends('layouts.app')

@section('content')
 @while (have_posts()) @php the_post() @endphp
    <div class="container">
            <div class="row">
                <div class="page-bg lazyload" data-background-image="{!! ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url'] )!!}">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-title">
                    <h1 id="p_title" class="tri-title page-title triafter tri-medium">{!!get_field('p_title') ? get_field('p_title') : get_the_title()!!}</h1>            
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-6 pr-3">
                    <div class="row">
                        <div class="col-12 col-content service-cost swiper-container reviews-swiper">
                           <div class="swiper-wrapper">
                            @foreach (get_field('reviews', 'options') as $item)
                                <div class="item-wrapper result review swiper-slide">
                                    <h3 class="tri-title default d2">{!!$item['name']!!}</h3>
                                    <div class="review-body">{!!$item['text']!!}</div>
                                    <div class="review-date">{!!$item['date']!!}</div>
                                </div>
                            @endforeach
                           </div>
                           <div class="swiper-scrollbar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 @endwhile
@endsection
