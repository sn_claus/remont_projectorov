@php

$query_args2 = array(
    'post_type' => 'uslugi',
    'posts_per_page' => -1,
    'orderby' => 'rand',
    'order' => 'ASC'
);
$query = new WP_Query( $query_args2 );
$uposts = $query->posts;
$counter = 0;
$page_id = get_field('p_services', 'options');
@endphp

@extends('layouts.app')

@section('content')
   
    @if(is_singular('uslugi'))
    <div class="container">
        <div class="row">
            <div class="page-bg lazyload" data-background-image="{!! ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url'] )!!}">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-title">
                <h1 id="p_title" class="tri-title page-title triafter tri-medium">{!!get_field('p_title') ? get_field('p_title') : get_the_title()!!}</h1>            </div>
        </div>
        <div class="row">
            <div class="col-12 col-xl-6">
                <div class="row">
                    @php $rec = true; @endphp
                    @if(get_field('reason'))
                        <div class="col-12 col-content">
                            {!!get_field('reason')!!}
                        </div>
                    @elseif(get_field('recomendation'))
                        <div class="col-12 col-content">
                            @php $rec = false; @endphp
                            {!!get_field('recomendation')!!}
                        </div>
                    @endif
                    <div class="col-12 service-cost">
                          <div class="item-wrapper result">
                              <span>Стоимость ремонта</span>
                              <span class="item-price">От {!! (get_field('cf1') ? get_field('cf1') : '000'); !!} Р</span>
                              <span class="result-garant">{{get_field('remont', $page_id)}}</span>
                          </div>
                    </div>
                    <div class="form-init col-12 form-id horizontal-form">
                        {!!do_shortcode('[contact-form-7 id="137" title="Order with ID"]');!!}
                    </div>
                    @if(get_field('recomendation') && $rec)
                        <div class="col-12 col-addcontent">
                            {!!get_field('recomendation')!!}
                        </div>
                    @endif
                </div>
            </div>
            @if (get_field('others'))
            <div class="col-12 col-xl-6 col-addservice">
                <div class="row row-services">
                    <div class="col-title col-12">
                        <a href="{!!get_permalink($page_id)!!}">Прочие услуги</a>
                    </div>
                    <div class="col-12 service-slide">
                        @foreach(get_field('others') as $post)
                            @if($counter<25) 
                                <div class="item-wrapper">

                                    <a href="{!!get_permalink($post->ID)!!}" class="item-title">
                                        {!!get_the_title($post->ID)!!}
                                    </a>
                                    <span class="item-price">от {!! (get_field('cf1', $post->ID) ? get_field('cf1', $post->ID) : '000'); !!} р</span>
                                    <button data-micromodal-trigger="order_id" data-title="{!!get_the_title($post->ID)!!}" class="btn default order-modal v2">Заказать</button>
                                </div>
                                @php $counter++@endphp
                            @else
                                @break
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

    @else
    @while(have_posts()) @php the_post() @endphp
    <div class="container">
        <div class="row">
            <div class="page-bg lazyload" data-background-image="{!! ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url'] )!!}">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-title">
                <h1 id="p_title" class="tri-title page-title triafter tri-medium">{!!get_field('p_title') ? get_field('p_title') : get_the_title()!!}</h1>            
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-xl-6">
                <div class="row">
                  <div class="col-12 col-content">
                      {!!get_the_content()!!}
                  </div>
                  @if (get_field("add_content"))
                    <div class="col-12 col-xl-6 col-addcontent">
                        {!!get_field("add_content")!!}
                    </div>
                  @endif
                </div>
            </div>
            <div class="col-12 col-xl-12 col-addservice">
                {{-- <div class="form-init col-12 form-id horizontal-form">
                    {!!do_shortcode('[contact-form-7 id="137" title="Order with ID"]');!!}
                </div> --}}
                <div class="row row-services">
                    <div class="col-title col-12 mb-0">
                        <a href="{!!get_permalink($page_id)!!}">Наши услуги</a>
                    </div>
                    <div class="carousel">
                        <div class="container">
                            <div class="row row-services swiper-container mt-4">
                                <div class="swiper-wrapper">
                                    <div class="col-12 col-lg-6 service-slide swiper-slide">
                                        @foreach($uposts as $post)
                                            @php if($counter%5==0 && $counter!==0) echo '</div><div class="col-12 col-lg-6 service-slide swiper-slide">'; @endphp
                                            <div class="item-wrapper">
                
                                                <a href="{!!get_permalink($post->ID)!!}" class="item-title">
                                                    {!!get_the_title($post->ID)!!}
                                                </a>
                                                <span class="item-price">от {!! (get_field('cf1', $post->ID) ? get_field('cf1', $post->ID) : '000'); !!} р</span>
                                                <button data-micromodal-trigger="order" class="btn default v2">Заказать</button>
                                            </div>
                                            @php $counter++; @endphp
                                        @endforeach
                                    </div>
                                </div>
                                <div class="swiper-scrollbar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endwhile
@endif

@endsection
