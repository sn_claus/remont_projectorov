<?php
$query_args = array(
    'post_type' => 'uslugi',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'ASC'
);
$query = new WP_Query( $query_args );
$uposts = $query->posts;

if ($_GET['set']) {
    if(get_field('set', 9)[intval($_GET['set'])]) {
        $set = intval($_GET['set']);
        $post_set = get_field('set', 9)[$set - 1];
        $query_args2 = array(
            'post_type' => 'uslugi',
            'posts_per_page' => -1,
            'orderby' => 'rand',
            'order' => 'ASC'
        );
        $query = new WP_Query( $query_args2 );
        $uposts = $query->posts;
    }
}



// foreach($uposts as $post):
//     if(!$post->post_parent){
//         $auslugi[] = $post->ID;
//     }
// endforeach;

// $auslugi = array_chunk($auslugi, 2);
$counter = 0;


?>

<?php $__env->startSection('content'); ?>
<?php if(!$post_set): ?>
  <?php while(have_posts()): ?> <?php the_post() ?>
  
    <div class="container">
        <div class="row">
            <div class="page-bg" style="background-image: url(<?php echo ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_template_directory_uri().'../assets/images/default-bg.png' ); ?>)">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-title">
                <h1 class="tri-title page-title triafter tri-medium"><?php echo (get_field('p_title') ? get_field('p_title') : get_title());; ?></h1>
            </div>
            <div class="col-xl-6 col-content">
                <?php echo $__env->make('partials.content-page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <?php if(get_field("add_content")): ?>
                <div class="col-12 col-xl-6 col-addcontent">
                    <?php echo get_field("add_content"); ?>

                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="carousel">
        <div class="container">
            <div class="row row-services swiper-container">
                <div class="swiper-wrapper">
                    <div class="col-12 col-lg-6 service-slide swiper-slide">
                        <?php $__currentLoopData = $uposts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($counter%5==0 && $counter!==0) echo '</div><div class="col-12 col-lg-6 service-slide swiper-slide">'; ?>
                            <div class="item-wrapper">

                                <a href="<?php echo get_permalink($post->ID); ?>" class="item-title">
                                    <?php echo get_the_title($post->ID); ?>

                                </a>
                                <span class="item-price">от <?php echo (get_field('cf1', $post) ? get_field('cf1', $post) : '000');; ?> р</span>
                                <button data-micromodal-trigger="order" class="btn default v2">Заказать</button>
                            </div>
                            <?php $counter++; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
<?php else: ?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-title">
                <h1 id="p_title" class="tri-title page-title triafter tri-medium"><?php echo ($post_set['p_title'] ? $post_set['p_title'] : $post_set['title']); ?></h1>            </div>
        </div>
        <div class="row">
            <div class="col-12 col-xl-6">
                <div class="row">
                    <?php $rec = true; ?>
                    <?php if($post_set['reason']): ?>
                        <div class="col-12 col-content">
                            <?php echo $post_set['reason']; ?>

                        </div>
                    <?php elseif($post_set['recomendation']): ?>
                        <div class="col-12 col-content">
                            <?php $rec = false; ?>
                            <?php echo $post_set['recomendation']; ?>

                        </div>
                    <?php endif; ?>
                    <div class="col-12 service-cost">
                        <?php $result=0; ?>
                        <?php $__currentLoopData = $post_set['items']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            
                            <div class="item-wrapper">
                                <a href="<?php echo get_permalink($item->ID); ?>" class="item-title"><?php echo $item->post_title; ?></a>
                                <span class="item-price">от <?php echo ($item->cf1 ? $item->cf1 : '000'); $result=intval($item->cf1)+$result;; ?> р</span>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <div class="item-wrapper result">
                                <span>Стоимость ремонта</span>
                                <span class="item-price">до <?php echo $result; ?> Р</span>
                                <span class="result-garant"><?php echo e(get_field('remont')); ?></span>
                            </div>
                    </div>
                    <div class="form-init col-12 form-id horizontal-form">
                        <?php echo do_shortcode('[contact-form-7 id="137" title="Order with ID"]');; ?>

                    </div>
                    <?php if($post_set['recomendation'] && $rec): ?>
                        <div class="col-12 col-addcontent">
                            <?php echo $post_set['recomendation']; ?>

                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-xl-6 col-addservice">
                <div class="row row-services">
                    <div class="col-title col-12">
                        <a href="<?php echo get_permalink(11); ?>">Прочие услуги</a>
                    </div>
                    <div class="col-12 service-slide">
                        <?php $__currentLoopData = $uposts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($counter<5): ?> 
                                <div class="item-wrapper">

                                    <a href="<?php echo get_permalink($post->ID); ?>" class="item-title">
                                        <?php echo get_the_title($post->ID); ?>

                                    </a>
                                    <span class="item-price">от <?php echo (get_field('cf1', $post) ? get_field('cf1', $post) : '000');; ?> р</span>
                                    <button data-micromodal-trigger="order_id" data-title="<?php echo get_the_title($post->ID); ?>" class="btn default order-modal v2">Заказать</button>
                                </div>
                                <?php $counter++?>
                            <?php else: ?>
                                <?php break; ?>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>