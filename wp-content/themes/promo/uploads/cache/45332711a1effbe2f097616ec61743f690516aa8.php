<?php $__env->startSection('content'); ?>
<?php while(have_posts()): ?> <?php the_post() ?>
<div class="container">
  <div class="row">
      <div class="page-bg" style="
        background-image: url(<?php echo ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url']); ?>

        )"></div>
    </div>
    <div class="row">
        <div class="col-12 col-title">
            <h1 id="p_title" class="tri-title page-title triafter tri-medium"><?php echo get_field('p_title') ? get_field('p_title') : get_the_title(); ?></h1>            
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xl-12">
            <div class="row">
              <div class="col-12 col-content <?php echo (get_field('add_content') && get_field("дополонительная_колонка") ? 'col-xl-6' : ''); ?>">
                  <?php echo get_the_content(); ?>

              </div>
              <?php if(get_field("add_content") && get_field("дополонительная_колонка")): ?>
                <div class="col-12 col-xl-6 col-addcontent">
                    <?php echo get_field("add_content"); ?>

                </div>
              <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>