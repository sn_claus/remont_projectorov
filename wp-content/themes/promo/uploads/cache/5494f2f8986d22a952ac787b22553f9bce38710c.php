<section class="order">
    <div class="container">
        <div class="row">
            <div class="d-none d-xl-flex col-6 bg-col">
                <img  class="img-fluid" alt="<?php echo e(get_field('order_bg', 'options')['alt']); ?>" src="<?php echo e(get_field('order_bg', 'options')['url']); ?>">
            </div>
            <div class="col-12 col-xl-6 form-col">
                <h3 class="tri-title triafter v2">закажите ремонт <b>в нашем сервисе</b></h3>
                <div class="form-body form-init">
                    <?php echo do_shortcode('[contact-form-7 id="89" title="Order"]');; ?>

                </div>
            </div>
        </div>
    </div>
</section>