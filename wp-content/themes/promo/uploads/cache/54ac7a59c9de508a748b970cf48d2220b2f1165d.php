<section class="troubles">
    <div class="container">
        <div class="row row-troubles">
            <?php $counter = 0; ?>
            <?php $__currentLoopData = get_field('set'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper">
                    <div class="item-title w-100">
                        <a  href="<?php echo get_permalink($item->ID); ?>" class="tri-title triafter tri-small"><?php echo get_the_title($item->ID); ?></a>
                    </div>
                    <div class="item-body w-100">
                        <?php if(get_field('others', $item->ID)): ?>
                            <?php $__currentLoopData = get_field('others', $item->ID); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($key > 2): ?> <?php break; ?>
                                <?php endif; ?>
                                <a href="<?php echo get_permalink($service->ID); ?>"><?php echo $service->post_title; ?></a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </div>
                    <div class="item-actions w-100">
                        <a class="btn default" href="<?php echo get_permalink($item->ID); ?>">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                        <span class="item-price">от <?php echo (get_field('cf1', $item->ID) ? get_field('cf1', $item->ID) : '000');; ?> Р</span>
                    </div>
                    <div class="item-count"><?php if($counter<9) {echo '0'.($counter + 1);} else {echo $counter + 1;}  $counter++; ?></div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>