<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="page-bg d-none d-lg-block" style="background-image: url(<?php echo ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_template_directory_uri().'../assets/images/default-bg.png' ); ?>)">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-title">
                <h1 id="p_title" class="tri-title page-title triafter tri-medium"><?php echo get_field('p_title') ? get_field('p_title') : get_the_title(); ?></h1>            
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                <div class="col-12 col-content">
                    <?php $__currentLoopData = get_field('brends'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="item-brand">
                            <div class="item-img">
                                <img alt="brand-img" class="img-fluid" src="<?php echo $item['img']; ?>">
                            </div>
                            <div class="item-content">
                                <p><?php echo $item['text']; ?></p>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <?php if(get_field("add_content")): ?>
                    <div class="col-12 col-xl-6 col-addcontent">
                        <?php echo get_field("add_content"); ?>

                    </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>