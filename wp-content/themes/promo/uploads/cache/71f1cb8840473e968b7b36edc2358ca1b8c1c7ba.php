<section class="troubles">
    <div class="container">
        <div class="row row-troubles">
            <?php $__currentLoopData = get_field('set'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper">
                    <div class="item-title w-100">
                        <a href="#" class="tri-title triafter tri-small"><?php echo $item['title']; ?></a>
                    </div>
                    <div class="item-body w-100">
                        <?php $__currentLoopData = $item['items']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a href="<?php echo get_permalink($service->ID); ?>"><?php echo $service->post_title; ?></a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div class="item-actions w-100">
                        <?php 
                        $args = array( 'set' => ($key + 1));
                        $target2 = get_permalink(11);
                        $target = add_query_arg( $args, $target2 );
                        ?>
                        <a class="btn default" href="<?php echo $target; ?>">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                        <span class="item-price">от 000 Р</span>
                    </div>
                    <div class="item-count">0<?php echo $key + 1; ?></div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>