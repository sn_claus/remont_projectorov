<header>
  <div class="container">
    <nav class="main-nav">
      <?php if(!is_front_page()): ?>
        <a class="logo-wrap" href="<?php echo get_home_url(); ?>">
            <img alt="logo-main" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/logo.png">
            <span class="logo-text">ремонт <b>проекторов</b></span>
        </a>
      <?php else: ?>
      <div class="logo-wrap">
          <img alt="logo-main" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/logo.png">
          <span class="logo-text">ремонт <b>проекторов</b></span>
      </div>
      <?php endif; ?>
      <div class="nav-menu-toggler d-xl-none order-3">
        <button type="button" class="toggle-nav">
          <span></span>
          <span class="less"></span>
          <span class="less"></span>
          <span></span>
        </button>
        </div>
      <div class="nav nav-menu d-none d-xl-block">
        <?php if(has_nav_menu('primary_navigation')): ?>
          <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new web_walker]); ?>

        <?php endif; ?>
      </div>
      <div class="nav-call order-2 order-xl-3 ml-auto ml-xl-0">
          <a class="phone-number" href="tel:<?php echo e(preg_replace('![^0-9]+!', '', get_field('tel', 'options'))); ?>"><?php echo e(get_field('tel', 'options')); ?>

            <img alt="call" class="call-img" src="<?= get_template_directory_uri(); ?>/assets/images/call.png">
          </a>
          <span class="phone-city d-block"><?php echo e(get_field('city', 'options')); ?></span>
      </div>
    </nav>
  </div>
</header>
