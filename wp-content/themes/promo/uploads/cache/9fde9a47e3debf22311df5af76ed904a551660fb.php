<section class="promotion">
    <div class="container">
        <div class="row">
            <div class="col-12 promo-wrapper">
                <h3 class="promo-title">Успей оставить заявку</h3>
                <div class="promo-body">
                    <?php echo $__env->make('frontpage.countdown', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <h3 class="promo-title-after">и получи скидку <b>500 рублей</b></h3>
                </div>
        </div>
    </div>
</section>