<section class="hero">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <h1 class="light-title section-title"><?php echo get_field('p_title'); ?></h1>
                <div class="hero-desc">
                    <?php echo get_the_content(); ?>

                </div>
                <button data-micromodal-trigger="order" class="btn light d-none d-xl-inline-block">Заказать ремонт 
                    <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                    <?php echo $__env->renderComponent(); ?>
                </button>
                <ul class="social-hero d-none d-xl-flex">
                    <li>
                        <a href="<?php echo get_field('vk', 'options'); ?>" target="_blank">
                            <?php $__env->startComponent('components.icon', ['name' => 'vk']); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_field('facebook', 'options'); ?>" target="_blank">
                            <?php $__env->startComponent('components.icon', ['name' => 'facebook']); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_field('odno', 'options'); ?>" target="_blank">
                            <?php $__env->startComponent('components.icon', ['name' => 'odnoklassniki']); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_field('twitter', 'options'); ?>" target="_blank">
                            <?php $__env->startComponent('components.icon', ['name' => 'twitter']); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_field('google', 'options'); ?>" target="_blank">
                            <?php $__env->startComponent('components.icon', ['name' => 'google-plus']); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-xl-6 tribefore d-flex flex-wrap xl-only">
                <div class="row add-info order-2 order-xl-1 d-none d-sm-flex">
                    <div class="col-auto">
                        <div class="line-before">Бесплатная<br> диагностика</div>
                    </div>
                    <div class="col-auto">
                        <div class="line-before">Гарантия<br> 1 год</div>
                    </div>
                    <div class="col-auto">
                        <div class="line-before">В наличии запчасти<br> для любых моделей</div>
                    </div>
                </div>
                <div class="row hero-img order-1 order-xl-2">
                    <img alt="hero-img" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/hero.png">
                </div>
                <div class="row order-3 d-xl-none text-center w-100 justify-content-center my-5">
                    <button data-micromodal-trigger="order" class="btn light">Заказать ремонт 
                        <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                        <?php echo $__env->renderComponent(); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>