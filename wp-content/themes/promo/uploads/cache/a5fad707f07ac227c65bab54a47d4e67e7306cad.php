<?php

$query_args2 = array(
    'post_type' => 'uslugi',
    'posts_per_page' => -1,
    'orderby' => 'rand',
    'order' => 'ASC'
);
$query = new WP_Query( $query_args2 );
$uposts = $query->posts;
$counter = 0;
?>


<?php $__env->startSection('content'); ?>
   
    <?php if(is_singular('uslugi')): ?>
    <div class="container">
        <div class="row">
            <div class="page-bg" style="background-image: url(<?php echo ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_template_directory_uri().'../assets/images/default-bg.png' ); ?>)">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-title">
                <h1 id="p_title" class="tri-title page-title triafter tri-medium"><?php echo get_field('p_title') ? get_field('p_title') : get_the_title(); ?></h1>            </div>
        </div>
        <div class="row">
            <div class="col-12 col-xl-6">
                <div class="row">
                    <?php $rec = true; ?>
                    <?php if(get_field('reason')): ?>
                        <div class="col-12 col-content">
                            <?php echo get_field('reason'); ?>

                        </div>
                    <?php elseif(get_field('recomendation')): ?>
                        <div class="col-12 col-content">
                            <?php $rec = false; ?>
                            <?php echo get_field('recomendation'); ?>

                        </div>
                    <?php endif; ?>
                    <div class="col-12 service-cost">
                          <div class="item-wrapper result">
                              <span>Стоимость ремонта</span>
                              <span class="item-price">От <?php echo (get_field('cf1') ? get_field('cf1') : '000');; ?> Р</span>
                              <span class="result-garant"><?php echo e(get_field('remont', 11)); ?></span>
                          </div>
                    </div>
                    <div class="form-init col-12 form-id horizontal-form">
                        <?php echo do_shortcode('[contact-form-7 id="137" title="Order with ID"]');; ?>

                    </div>
                    <?php if(get_field('recomendation') && $rec): ?>
                        <div class="col-12 col-addcontent">
                            <?php echo get_field('recomendation'); ?>

                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-xl-6 col-addservice">
                <div class="row row-services">
                    <div class="col-title col-12">
                        <a href="<?php echo get_permalink(11); ?>">Прочие услуги</a>
                    </div>
                    <div class="col-12 service-slide">
                        <?php $__currentLoopData = $uposts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($counter<5): ?> 
                                <div class="item-wrapper">

                                    <a href="<?php echo get_permalink($post->ID); ?>" class="item-title">
                                        <?php echo get_the_title($post->ID); ?>

                                    </a>
                                    <span class="item-price">от <?php echo (get_field('cf1', $post) ? get_field('cf1', $post) : '000');; ?> р</span>
                                    <button data-micromodal-trigger="order_id" data-title="<?php echo get_the_title($post->ID); ?>" class="btn default order-modal v2">Заказать</button>
                                </div>
                                <?php $counter++?>
                            <?php else: ?>
                                <?php break; ?>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php else: ?>
    <?php while(have_posts()): ?> <?php the_post() ?>
    <div class="container">
        <div class="row">
            <div class="page-bg" style="background-image: url(<?php echo ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_template_directory_uri().'../assets/images/default-bg.png' ); ?>)">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-title">
                <h1 id="p_title" class="tri-title page-title triafter tri-medium"><?php echo get_field('p_title') ? get_field('p_title') : get_the_title(); ?></h1>            
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-xl-6">
                <div class="row">
                  <div class="col-12 col-content">
                      <?php echo get_the_content(); ?>

                  </div>
                  <?php if(get_field("add_content")): ?>
                    <div class="col-12 col-xl-6 col-addcontent">
                        <?php echo get_field("add_content"); ?>

                    </div>
                  <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-xl-12 col-addservice">
                
                <div class="row row-services">
                    <div class="col-title col-12 mb-0">
                        <a href="<?php echo get_permalink(11); ?>">Наши услуги</a>
                    </div>
                    <div class="carousel">
                        <div class="container">
                            <div class="row row-services swiper-container mt-4">
                                <div class="swiper-wrapper">
                                    <div class="col-12 col-lg-6 service-slide swiper-slide">
                                        <?php $__currentLoopData = $uposts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($counter%5==0 && $counter!==0) echo '</div><div class="col-12 col-lg-6 service-slide swiper-slide">'; ?>
                                            <div class="item-wrapper">
                
                                                <a href="<?php echo get_permalink($post->ID); ?>" class="item-title">
                                                    <?php echo get_the_title($post->ID); ?>

                                                </a>
                                                <span class="item-price">от <?php echo (get_field('cf1', $post) ? get_field('cf1', $post) : '000');; ?> р</span>
                                                <button data-micromodal-trigger="order" class="btn default v2">Заказать</button>
                                            </div>
                                            <?php $counter++; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                                <div class="swiper-scrollbar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>