<section class="hero">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <h1 class="light-title section-title"><?php echo get_field('p_title'); ?></h1>
                <div class="hero-desc">
                    <?php echo get_the_content(); ?>

                </div>
                <button data-micromodal-trigger="order" class="btn light d-none d-xl-inline-block">Заказать ремонт 
                    <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                    <?php echo $__env->renderComponent(); ?>
                </button>
                <ul class="social-hero d-none d-xl-flex">
                    <?php if(in_array("vk", get_field('a_soc', 'options'))): ?>
                        <li>
                            <a href="<?php echo get_field('vk', 'options'); ?>" target="_blank">
                                <?php $__env->startComponent('components.icon', ['name' => 'vk']); ?>
                                <?php echo $__env->renderComponent(); ?>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if(in_array("facebook", get_field('a_soc', 'options'))): ?>
                    <li>
                        <a href="<?php echo get_field('facebook', 'options'); ?>" target="_blank">
                            <?php $__env->startComponent('components.icon', ['name' => 'facebook']); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if(in_array("Однокласники", get_field('a_soc', 'options'))): ?>
                    <li>
                        <a href="<?php echo get_field('odno', 'options'); ?>" target="_blank">
                            <?php $__env->startComponent('components.icon', ['name' => 'odnoklassniki']); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if(in_array("twitter", get_field('a_soc', 'options'))): ?>
                    <li>
                        <a href="<?php echo get_field('twitter', 'options'); ?>" target="_blank">
                            <?php $__env->startComponent('components.icon', ['name' => 'twitter']); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if(in_array("google-plus", get_field('a_soc', 'options'))): ?>
                    <li>
                        <a href="<?php echo get_field('google', 'options'); ?>" target="_blank">
                            <?php $__env->startComponent('components.icon', ['name' => 'google-plus']); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-12 col-xl-6 tribefore d-flex flex-wrap xl-only fix-bg">
                <div class="row add-info order-2 order-xl-1 d-none d-sm-flex">
                    <?php if(get_field('hero_links', 'options')): ?>
                        <?php $__currentLoopData = get_field('hero_links', 'options'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-auto">
                                <a class="line-before" href="<?php echo get_permalink($item['hero_link']->ID); ?>"><?php echo $item['text_link']; ?></a>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
                <div class="row hero-img order-1 order-xl-2">
                    <img  class="logo-img" alt="<?php echo e(get_field('hero', 'options')['alt']); ?>" src="<?php echo e(get_field('hero', 'options')['url']); ?>">
                </div>
                <div class="row order-3 d-xl-none text-center w-100 justify-content-center my-5">
                    <button data-micromodal-trigger="order" class="btn light">Заказать ремонт 
                        <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                        <?php echo $__env->renderComponent(); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>