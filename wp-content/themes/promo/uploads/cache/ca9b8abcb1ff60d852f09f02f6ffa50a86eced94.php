<?php
$terms = get_terms([
    'taxonomy' => 'brands',
    'hide_empty' => false,
    'parent' => 0,
]);
?>
<?php $__env->startSection('content'); ?>
<?php while(have_posts()): ?> <?php the_post() ?>
    <div class="container">
        <div class="row">
            <div class="page-bg d-none d-lg-block" style="background-image: url(<?php echo ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url'] ); ?>)">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-title">
                <h1 id="p_title" class="tri-title page-title triafter tri-medium"><?php echo get_field('p_title') ? get_field('p_title') : get_the_title(); ?></h1>            
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 col-content">
                        
                        <?php $__currentLoopData = $terms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a class="item-brand" href="<?php echo get_term_link($item->term_id); ?>">
                                <div class="item-img">
                                    <img alt="<?php echo get_field('img', 'brands_'.$item->term_id)['alt']; ?>" class="img-fluid" title="<?php echo e(get_field('img', 'brands_'.$item->term_id)['title']); ?>" src="<?php echo e(get_field('img', 'brands_'.$item->term_id)['url']); ?>">
                                </div>
                                <div class="item-content">
                                    <p><?php echo term_description($item->term_id); ?></p>
                                </div>
                            </a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php if(get_field("add_content")): ?>
                        <div class="col-12 col-xl-6 col-addcontent">
                            <?php echo get_field("add_content"); ?>

                        </div>
                    <?php endif; ?>
                </div>
                <div class="row">
                    <div class="col-12 col-addcontent">
                        <?php echo get_the_content(); ?>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>