<?php $__env->startSection('content'); ?>
<?php while(have_posts()): ?> <?php the_post() ?>
    <div class="container fix-bg">
        <div class="row d-none d-xl-flex">
            <div class="page-bg">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-xl-6 col-content">
                <div class="row">
                    <div class="col-12 col-title">
                        <h1 id="p_title" class="tri-title page-title triafter tri-medium"><?php echo get_field('p_title') ? get_field('p_title') : get_the_title(); ?></h1>            
                    </div>
                    <div class="col-12 col-content">
                        <?php echo get_the_content(); ?>

                    </div>
                    <div class="col-sm-6 col-12 col-addcontent">
                        <span class="blue-text">тел: <a class="white-link" href="tel:<?php echo e(preg_replace('![^0-9]+!', '', get_field('tel', 'options'))); ?>"><?php echo get_field('tel', 'options'); ?></a></span>
                        <p class="blue-text"><?php echo get_field('time_work', 'options'); ?></p>
                    </div>
                    <div class="col-sm-6 col-12 col-addcontent">
                        <span class="blue-text">Центральный офис:</span>
                        <p><?php echo get_field('office', 'options'); ?></p>
                    </div>
                </div>
            </div>
            <?php if(get_field("add_content")): ?>
                <div class="col-12 col-xl-6 col-addcontent map">
                    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                    <script>
                        var YaMapsWP = {}, YMlisteners = {};
                    </script>
                    <?php echo get_field("map", "options"); ?>

                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>