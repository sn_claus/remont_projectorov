<footer class="floor">
  <div class="nav-footer w-100">
    <div class="container">
      <div class="floor-nav d-none d-xl-flex row">
        <nav class="main-nav">
            <?php if(!is_front_page()): ?>
              <a class="logo-wrap" href="<?php echo get_home_url(); ?>">
                  <img alt="logo-main" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/logo.png">
                  <span class="logo-text">ремонт <b>проекторов</b></span>
              </a>
            <?php else: ?>
            <div class="logo-wrap">
                <img alt="logo-main" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/logo.png">
                <span class="logo-text">ремонт <b>проекторов</b></span>
            </div>
            <?php endif; ?>
            <div class="nav-menu-toggler d-xl-none order-3">
              <button type="button" class="toggle-nav">
                <span></span>
                <span class="less"></span>
                <span class="less"></span>
                <span></span>
              </button>
              </div>
            <div class="nav nav-menu d-none d-xl-block">
              <?php if(has_nav_menu('primary_navigation')): ?>
                <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new web_walker]); ?>

              <?php endif; ?>
            </div>
            <button data-micromodal-trigger="order" class="btn light">Заказать ремонт 
                <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                <?php echo $__env->renderComponent(); ?>
            </button>
          </nav>
      </div>
    </div>
  </div>
  <div class="main-floor w-100">
    <div class="container">
      <div class="row">
        <a class="phone-number" href="tel:<?php echo e(preg_replace('![^0-9]+!', '', get_field('tel', 'options'))); ?>"><?php echo e(get_field('tel', 'options')); ?>

        </a>
        <span class="copyright"><?php echo e(get_field('copyright', 'options')); ?></span>
        <div class="social-floor">
            <ul class="social-group d-flex">
                <li>
                    <a href="<?php echo get_field('vk', 'options'); ?>" target="_blank">
                        <?php $__env->startComponent('components.icon', ['name' => 'vk']); ?>
                        <?php echo $__env->renderComponent(); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo get_field('facebook', 'options'); ?>" target="_blank">
                        <?php $__env->startComponent('components.icon', ['name' => 'facebook']); ?>
                        <?php echo $__env->renderComponent(); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo get_field('odno', 'options'); ?>" target="_blank">
                        <?php $__env->startComponent('components.icon', ['name' => 'odnoklassniki']); ?>
                        <?php echo $__env->renderComponent(); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo get_field('twitter', 'options'); ?>" target="_blank">
                        <?php $__env->startComponent('components.icon', ['name' => 'twitter']); ?>
                        <?php echo $__env->renderComponent(); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo get_field('google', 'options'); ?>" target="_blank">
                        <?php $__env->startComponent('components.icon', ['name' => 'google-plus']); ?>
                        <?php echo $__env->renderComponent(); ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
  </div>
  <div class="nav-mobile">
      <?php if(has_nav_menu('primary_navigation')): ?>
        <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'items_wrap' => '<ul id="%1$s" class="nav %2$s">%3$s</ul>', 'container'=> false,'walker' => new web_walker]); ?>

      <?php endif; ?>
    </div>
    <div class="other-stuff"></div>
      <div class="micro_modal" id="order" aria-hidden="true">
          <div class="modal_overlay d-flex justify-content-center align-items-center" tab-index="-1" data-micromodal-close>
            <div class="modal_container triafter v2" role="dialog" aria-modal="true" aria-labelledby="order">
              <div class="modal-header d-flex justify-content-center align-items-center">
                <button class="modal-close toggle-nav opened" aria-label="Close modal" data-micromodal-close >
                    <span class="shape d-none"></span>
                    <span class="shape"></span>
                    <span class="shape"></span>
                    <span class="shape d-none"></span>
                </button>
                <span class="close-text">закрыть</span>
              </div>
                <div class="modal-content justify-content-center">
                  <h3 class="tri-title default">Заказ <b>ремонта</b></h3>
                  <p>Укажите Ваш номер телефона <br>и наш специалист перезвонит Вам в течении минуты. </p>
                  <div class="form-init">
                    <?php echo do_shortcode('[contact-form-7 id="89" title="Order"]');; ?>

                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="micro_modal" id="order_id" aria-hidden="true">
            <div class="modal_overlay d-flex justify-content-center align-items-center" tab-index="-1" data-micromodal-close>
              <div class="modal_container triafter v2" role="dialog" aria-modal="true" aria-labelledby="order">
                <div class="modal-header d-flex justify-content-center align-items-center">
                  <button class="modal-close toggle-nav opened" aria-label="Close modal" data-micromodal-close >
                      <span class="shape d-none"></span>
                      <span class="shape"></span>
                      <span class="shape"></span>
                      <span class="shape d-none"></span>
                  </button>
                  <span class="close-text">закрыть</span>
                </div>
                  <div class="modal-content justify-content-center">
                    <h3 class="tri-title default">Заказ <b>ремонта</b></h3>
                    <p>Укажите Ваш номер телефона <br>и наш специалист перезвонит Вам в течении минуты. </p>
                    <div class="form-init">
                      <?php echo do_shortcode('[contact-form-7 id="143" title="Order with ID (modal)"]');; ?>

                    </div>
                  </div>
                </div>
              </div>
          </div>
        <div class="mdc-snackbar mdc-snackbar--leading">
            <div class="mdc-snackbar__surface">
              <div class="mdc-snackbar__label" role="status" aria-live="polite">
              </div>
            </div>
        </div>
    </div>
</footer>