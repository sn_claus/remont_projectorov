<?php
    $query_args2 = array(
    'post_type' => 'post',
    'posts_per_page' => -1,
    'category' => 'benefits',
    'orderby' => 'date',
    'order' => 'ASC'
);
$query = new WP_Query( $query_args2 );
$uposts = $query->posts;
$counter = 0;
$bg= get_template_directory_uri().'/assets/images/benbg.png';
?>


<section class="benefits">
    <div class="container">
        <div class="row ben-row">
            <?php $__currentLoopData = $uposts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="item-wrapper triafter">
                        <div class="item-title">                       
                            <h2 class="tri-title multi"><?php echo (get_field('p_title', $post->ID) ? get_field('p_title', $post->ID) : $post->post_title); ?></h2>
                        </div>
                        <div class="item-body linebefore">
                            <?php echo $post->post_content; ?>

                        </div>
                        <div class="item-actions">
                            <a href="<?php echo (get_field('parent', $post->ID)? get_permalink(get_field('parent', $post->ID)) : get_permalink($post->ID)); ?>" class="btn default">
                                <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                                <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                            </a>
                        </div>
                        <div class="item-bg">
                            <img alt="item-bg" class="img-fluid" src="<?php echo ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : $bg ); ?>">
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>