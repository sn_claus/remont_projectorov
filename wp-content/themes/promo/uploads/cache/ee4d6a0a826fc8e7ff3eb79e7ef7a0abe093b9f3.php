<?php $__env->startSection('content'); ?>
<?php while(have_posts()): ?> <?php the_post() ?>
<div class="container">
  <div class="row">
      <div class="page-bg" style="
        background-image: url(<?php echo ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('default_bg', 'options')['url']); ?>

        )"></div>
    </div>
    <div class="row">
        <div class="col-12 col-title">
            <h1 id="p_title" class="tri-title page-title triafter tri-medium"><?php echo get_field('p_title') ? get_field('p_title') : get_the_title(); ?></h1>            
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xl-12">
            <div class="row">
                <div class="col-12 col-content <?php echo (get_field('add_content') && get_field("дополонительная_колонка") ? 'col-xl-6' : ''); ?>">
                    <?php echo get_the_content(); ?>

                    <div class="form-init d-xxl-none col-12 form-id horizontal-form big">
                        <?php echo do_shortcode('[contact-form-7 id="137" title="Order with ID"]');; ?>

                    </div>
                    <div class="col-masters">
                        <h3>Лучшие мастера</h3>
                        <div class="swiper-container masters-carousel">
                            <div class="swiper-wrapper">
                                <?php $__currentLoopData = get_field('masters'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="swiper-slide">
                                        <div class="img-wrap">
                                            <img alt="<?php echo $item['img']['alt']; ?>" class="img-fluid" title="<?php echo e($item['img']['title']); ?>" src="<?php echo e($item['img']['url']); ?>">
                                        </div>
                                        <div class="item-title"><?php echo $item['full']['name']; ?></div>
                                        <div class="item-body"><?php echo $item['full']['desc']; ?></div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <div class="col-addcontent">
                            <?php echo get_field('content_about'); ?>

                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-6 col-addcontent d-none d-xxl-block">
                    <?php if(get_field("add_content")): ?>
                        <?php echo get_field("add_content"); ?>

                    <?php endif; ?>
                    <div class="form-init d-none d-xl-block col-12 form-id horizontal-form">
                        <?php echo do_shortcode('[contact-form-7 id="137" title="Order with ID"]');; ?>

                    </div>
                </div>
              
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>