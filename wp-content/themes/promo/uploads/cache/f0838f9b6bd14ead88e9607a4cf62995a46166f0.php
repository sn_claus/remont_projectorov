<?php $__env->startSection('content'); ?>
 <?php while(have_posts()): ?> <?php the_post() ?>
    <div class="container">
            <div class="row">
            <div class="page-bg" style="
                background-image: url(<?php echo ( get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_template_directory_uri().'../assets/images/default-bg.png' ); ?>

                )"></div>
            </div>
            <div class="row">
                <div class="col-12 col-title">
                    <h1 id="p_title" class="tri-title page-title triafter tri-medium"><?php echo get_field('p_title') ? get_field('p_title') : get_the_title(); ?></h1>            
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-6">
                    <div class="row">
                        <div class="col-12 col-content service-cost">
                            <?php $__currentLoopData = get_field('reviews', 'options'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="item-wrapper result review">
                                    <h3 class="tri-title default d2"><?php echo $item['name']; ?></h3>
                                    <div class="review-body"><?php echo $item['text']; ?></div>
                                    <div class="review-date"><?php echo $item['date']; ?></div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>