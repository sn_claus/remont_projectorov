<section class="our-service" id="our_service">
    <?php $__env->startComponent('components.section-title', ['title' => 'Our Service', 'subtitle' => 'what we do']); ?>
    <?php echo $__env->renderComponent(); ?>
    <div class="service-items">
        <div class="container">
            <div class="row">
                <?php $__currentLoopData = $our_service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item col-xl-3 col-6" data-aos="fade" data-aos-duration="800" data-aos-delay=<?php echo e(($key+1)*400); ?> data-aos-once="true">
                        <div class="item-img">
                            <img class="img-fluid lazyload" src="#" data-src="<?php echo e($item->icon); ?>">
                            <div class="swiper-lazy-preloader"></div>
                        </div>
                        <h3 class="item-title"><?php echo e($item->title); ?></h3>
                        <span class="item-desc"><?php echo e($item->text); ?></span>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</section>