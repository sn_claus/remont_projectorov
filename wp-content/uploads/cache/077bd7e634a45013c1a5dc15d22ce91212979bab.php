<header>
  <div class="container">
    <nav class="main-nav">
      <?php if(!is_front_page()): ?>
        <a class="logo-wrap" href="<?php echo get_home_url(); ?>">
            <img alt="<?php echo e(get_field('logo', 'options')['alt']); ?>" class="logo-img" title="<?php echo e(get_field('logo', 'options')['title']); ?>" src="<?php echo e(get_field('logo', 'options')['url']); ?>">
            <span class="logo-text"><?php echo get_field('logo_text', 'options'); ?></span>
        </a>
      <?php else: ?>
      <div class="logo-wrap">
            <img  class="logo-img" alt="<?php echo e(get_field('logo', 'options')['alt']); ?>" title="<?php echo e(get_field('logo', 'options')['title']); ?>" src="<?php echo e(get_field('logo', 'options')['url']); ?>">
            <span class="logo-text"><?php echo get_field('logo_text', 'options'); ?></span>
      </div>
      <?php endif; ?>
      <div class="nav-menu-toggler d-xl-none order-3">
        <button type="button" class="toggle-nav">
          <span></span>
          <span class="less"></span>
          <span class="less"></span>
          <span></span>
        </button>
        </div>
      <div class="nav nav-menu d-none d-xl-block">
        <?php if(has_nav_menu('primary_navigation')): ?>
          <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new web_walker]); ?>

        <?php endif; ?>
      </div>
      <div class="nav-call order-2 order-xl-3 ml-auto ml-xl-0">
          <a class="phone-number"  href="tel:<?php echo e(preg_replace('![^0-9]+!', '', get_field('tel', 'options'))); ?>"><?php echo e(get_field('tel', 'options')); ?>

            <img alt="call" class="call-img" src="<?= get_template_directory_uri(); ?>/assets/images/call.png">
          </a>
          <span class="phone-city d-block"><?php echo e(get_field('city', 'options')); ?></span>
      </div>
    </nav>
  </div>
  <style>
    html {
      --primary-color1: <?php echo get_field('color1', 'options'); ?>;
      --primary-color2: <?php echo get_field('color2', 'options'); ?>;
      --primary-color3: <?php echo get_field('color3', 'options'); ?>;
      --secondary-color1: <?php echo get_field('color4', 'options'); ?>;
      --text-color1: <?php echo get_field('text_color', 'options'); ?>;
      --text-color2: <?php echo get_field('text_color2', 'options'); ?>;
    }
    section.promotion::before,section.promotion::after {
      background-image: url("<?php echo get_field('promotion', 'options')['url']; ?>");
    }

  </style>
</header>
