<?php $__env->startSection('content'); ?>
<?php while(have_posts()): ?> <?php the_post() ?>
  <?php echo $__env->make('partials.home.slider-home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('partials.home.our-services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('partials.home.our-works', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('partials.home.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('partials.home.testimonials', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endwhile; ?>
  
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>