<?php $__env->startSection('content'); ?>
  <?php while(have_posts()): ?> <?php the_post() ?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-title">
                <h1 class="tri-title page-title triafter tri-medium"><?php echo (get_field('p_title') ? get_field('p_title') : get_title());; ?></h1>
            </div>
            <div class="col-xl-6 col-content">
                <?php echo $__env->make('partials.content-page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <?php if(get_field("add_content")): ?>
                <div class="col-12 col-xl-6 col-addcontent">
                    <?php echo get_field("add_content"); ?>

                </div>
            <?php endif; ?>
        </div>
    </div>
    
  <?php endwhile; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>