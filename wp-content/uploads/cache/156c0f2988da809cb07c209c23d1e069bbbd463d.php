<section class="hero" id="home">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="hero-title" >
                    <div class="aos-controller">
                        <span data-aos="slide-up" data-aos-duration="800" data-aos-delay="400" data-aos-once="true"><?php echo e(pll__('Your Virtual')); ?></span>
                    </div>
                    <div class="aos-controller">
                        <span data-aos="slide-up" data-aos-duration="800" data-aos-delay="200" data-aos-once="true"><?php echo e(pll__('Office')); ?></span>
                    </div>
                </div>
                <h1 class="d-none"><?php echo e(pll__('Your Virtual office')); ?></h1>
                <div class="aos-controller" data-aos="fade" data-aos-duration="800" data-aos-delay="800" data-aos-once="true">
                    <span class="hero-desc d-block" ><?php echo e(pll__('The flexible solution')); ?></span>
                    <a href="#offers" class="btn light"><?php echo e(pll__('See Offer')); ?></a>
                </div>
            </div>
            <div class="col-md-5 d-none d-lg-block">
                <img src="#!" data-aos="fade-left" data-aos-duration="800" data-aos-delay="400" data-aos-once="true" alt="virtual office" class="lazyload img-fluid" data-src="<?= App\asset_path('images/hero.png'); ?>">
                <div class="swiper-lazy-preloader"></div>
            </div>
        </div>
    </div>
</section>