<?php 
    $parent_id = get_field('blog', 'options');
    $args = array('posts_per_page' => 3, 'post_type' => 'post', 'category_name'=> 'blog');
    $posts = new WP_Query($args);
?>

<section class="blog inheritance our-services webvision left">
    <div class="head-slider">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 text-center">
                    <h2 class="section-title">Design Blog</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="main-layout v2">
        <div class="swiper-container">
            <div class="row-items row">
                <?php while($posts->have_posts()): ?> <?php $posts->the_post() ?>
                <div class="col-item swiper-slide col-lg-4 col-12 col-sm-6">
                    <div class="item-wrap">
                        <div class="item-img">
                             <?php echo the_post_thumbnail('blog', array('class' => 'img-fluid lazyload', 'title' => get_the_title())); ?>

                            <div class="swiper-lazy-preloader"></div>
                        </div>
                        <div class="item-title">
                            <h3><?php echo get_the_title(); ?></h3>
                        </div>
                        <div class="item-content">
                            <span class="date"><?php the_time('F d, Y'); ?></span>
                            <a href="<?php echo get_permalink(); ?>" class="no-btn">Read more</a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="row-actions">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <a href="<?php echo get_permalink($parent_id); ?>" class="btn btn-default"><span>All blog post</span></a>
                </div>
            </div>
        </div>
    </div>
</section>