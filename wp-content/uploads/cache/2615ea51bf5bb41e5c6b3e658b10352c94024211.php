<?php 
    $parent_id = get_field('blog', 'options');
    $args = array('posts_per_page' => -1, 'post_type' => 'post', 'category_name'=> 'blog');
    $posts = new WP_Query($args);
?>

<section class="blog inheritance our-services">
    <div class="head-slider">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 text-center">
                    <h2 class="section-title">Design Blog</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="main-layout v2">
        <div class="swiper-container">
            <div class="row-items swiper-wrapper">
                <?php while($posts->have_posts()): ?> <?php $posts->the_post() ?>
                <div class="col-item swiper-slide">
                    <div class="item-wrap">
                        <div class="item-img">
                             <?php echo e(the_post_thumbnail('blog', array('class' => 'img-fluid lazyload', 'title' => get_the_title()))); ?>

                            <div class="swiper-lazy-preloader"></div>
                        </div>
                        <div class="item-title">
                            <h3><?php echo e(get_the_title()); ?></h3>
                        </div>
                        <div class="item-content">
                            <span class="date"><?php the_time('F d, Y'); ?></span>
                            <a href="<?php echo e(get_permalink()); ?>" class="no-btn">Read more</a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="swiper-button-prev main v2"></div>
        <div class="swiper-button-next main v2"></div>
    </div>
    <div class="row-actions">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <a href="<?php echo e(get_permalink($parent_id)); ?>" class="btn btn-default"><span>All blog post</span></a>
                </div>
            </div>
        </div>
    </div>
</section>