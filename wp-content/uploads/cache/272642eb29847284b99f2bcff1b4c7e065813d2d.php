<?php 
    $parent_id = get_field('our_works', 'options');
    $fav_posts = get_field('fav', $parent_id);
    global $post;
?>

<section class="our-works">
    <div class="head-slider">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 text-center">
                    <h2 class="section-title">Our Works</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="gallery-slider mobile-only">
        <div class="container swiper-container">
            <div class="row-items row">
                    
                <?php $__currentLoopData = $fav_posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                    <?php setup_postdata($post) ?>
                    <div class="col-item col-md-4 col-sm-6 col-xs-12 swiper-slide">
                    <div class="img-wrap" data-toggle="modal" data-id="<?php echo the_ID(); ?>" data-target="#gallery">
                            <?php echo the_post_thumbnail('our-works', array('class' => 'img-fluid lazyload', 'title' => get_the_title())); ?>

                            <div class="swiper-lazy-preloader"></div>
                            <i class="fal fa-search"></i>
                            <div class="work-about">
                                <h2><?php echo the_title(); ?></h2>
                            <span><?php echo get_field('desc'); ?></span>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php wp_reset_postdata() ?>
            </div>
        </div>
    </div>
    <div class="row-actions">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <a href="<?php echo get_permalink($parent_id); ?>" class="btn btn-default"><span>All project</span></a>
                </div>
            </div>
        </div>
    </div>
</section>