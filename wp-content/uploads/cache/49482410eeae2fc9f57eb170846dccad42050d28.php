<div class="single-post">
  <div class="container">
    <article <?php echo post_class(); ?>>
      <div class="entry-content">
        <div class="head">
          <h1><?php echo e(get_the_title()); ?></h1>
          <div class="thumb-wrap">
            <?php echo e(the_post_thumbnail('single', array('class' => 'img-fluid lazyload thum-img', 'title' => get_the_title()))); ?>

          </div>
        </div>
        <div class="post-content">
          <?php the_content() ?>
        </div>
      </div>
    </article>
  </div>
</div>
