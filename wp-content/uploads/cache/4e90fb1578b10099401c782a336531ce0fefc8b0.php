<?php $phone = preg_replace('/\D/', '', $contacts->phone) ?>
<section class="contact-us" id="contact_us">
        <?php $__env->startComponent('components.section-title', ['title' => 'contact us','subtitle' => 'more information']); ?>
        <?php echo $__env->renderComponent(); ?>
    <div class="contact-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xl-3 off-set-xl-1 col-12">
                    <img src="#" class="lazyload img-fluid" data-aos="fade-up" data-aos-duration="800" data-aos-delay="400" data-aos-once="true" data-src="<?= App\asset_path('images/logo.png'); ?>" alt="logo">
                    <div class="contacts-info mb-5 mb-md-5">
                        <div class="single-info" data-aos="fade-up" data-aos-duration="800" data-aos-delay="600" data-aos-once="true">
                            <img src="#" data-src="<?= App\asset_path('images/place.png'); ?>" class="img-fluid lazyload" alt="contact-icon">
                            <div class="swiper-lazy-preloader"></div>
                            <span><?php echo $contacts->place; ?></span>
                        </div>
                    <a class="single-info" href="tel:<?php echo e($phone); ?>" data-aos="fade-up" data-aos-duration="800" data-aos-delay="800" data-aos-once="true">
                            <img src="#" data-src="<?= App\asset_path('images/phone.png'); ?>" class="img-fluid lazyload" alt="contact-icon">
                            <div class="swiper-lazy-preloader"></div>
                            <span><?php echo $contacts->phone; ?></span>
                        </a>
                        <a class="single-info" href="mailto:<?php echo $contacts->mail; ?>" data-aos="fade-up" data-aos-duration="800" data-aos-delay="1000" data-aos-once="true">
                            <img src="#" data-src="<?= App\asset_path('images/mail.png'); ?>" class="img-fluid lazyload" alt="contact-icon">
                            <div class="swiper-lazy-preloader"></div>
                            <span><?php echo $contacts->mail; ?></span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6 offset-lg-1 offset-xl-2 col-12">
                    <div class="form-body form-init row">
                        <?php if($current_lang === "English"): ?>
                            <?php echo do_shortcode('[contact-form-7 id="87" title="feedback(en)"]'); ?>

                        <?php else: ?> <?php echo do_shortcode('[contact-form-7 id="88" title="feedback(de)"]');; ?>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
</section>