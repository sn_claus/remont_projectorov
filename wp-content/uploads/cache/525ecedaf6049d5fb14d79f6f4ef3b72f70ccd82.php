<?php $__env->startSection('content'); ?>
<div class="page404">
  <?php echo $__env->make('partials.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="content-img">
    <img src="#" class="img-fluid lazyload m-auto d-block" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/404.png">
    <a href="<?php echo get_home_url(); ?>" class="btn btn-default mt-5">Go home</a>
  </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>