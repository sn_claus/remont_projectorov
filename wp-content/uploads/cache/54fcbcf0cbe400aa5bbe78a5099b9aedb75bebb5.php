<?php 
    $parent_id = get_field('blog', 'options');
    $form = get_field('contacts_us_form', 'options')
?>


<?php $__env->startSection('content'); ?>
<?php while(have_posts()): ?> <?php the_post() ?>
<section class="contactus page single-page">
    <div class="head-slider">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 text-center">
                    <h1 class="section-title before main-title">Contact us</h1>
                </div>
            </div>
        </div>
    </div>
    <div class='main-content'>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 col-lg-6 form-init">
                    <?php echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); ?>

                </div>
                <div class="col-12 col-md-5 col-lg-6 d-flex flex-wrap">
                    <div class="email-img d-none d-md-block">
                        <img alt="contact us" src="#" class="lazyload img-fluid" data-src="<?php echo e(get_template_directory_uri()); ?>/assets/images/contact.png">
                    </div>
                    <div class="contacts w-100">
                        <div class="contact-item">
                            <img class="lazyload img-fluid" src="#" data-src="<?php echo e(get_template_directory_uri()); ?>/assets/images/phone.png">
                            <a href="tel:<?php echo $phone; ?>"><?php echo e(get_field('phone','options')); ?></a>
                        </div>
                        <div class="contact-item">
                            <img class="lazyload img-fluid" src="#" data-src="<?php echo e(get_template_directory_uri()); ?>/assets/images/email.png">
                            <a href="mailto:<?php echo e(get_field('email','options')); ?>"><?php echo e(get_field('email','options')); ?></a>
                        </div>
                        <div class="contact-item">
                            <img class="lazyload img-fluid" src="#" data-src="<?php echo e(get_template_directory_uri()); ?>/assets/images/geo.png">
                            <span style="margin-top: 30px;color: #444;font-family: Proxima Nova;font-size: 14px;max-width: 125px;"><?php echo e(get_field('cord','options')); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php endwhile; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>