<header>
    <div class="container">
    <nav class="main-nav">
      <div class="logo-wrap">
          <img alt="logo-hnj" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/logo.png">
      </div>
      <div class="nav-menu-toggler d-md-none">
          <button type="button" class="toggle-nav">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </button>
        </div>
      <div class="nav nav-menu d-none d-md-block">
        <?php if(has_nav_menu('primary_navigation')): ?>
          <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new web_walker]); ?>

        <?php endif; ?>
      </div>
    </nav>
  </div>
</header>
