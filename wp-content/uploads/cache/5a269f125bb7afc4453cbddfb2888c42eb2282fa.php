

<?php 
    $parent_id = get_field('blog', 'options');
    $args = array('posts_per_page' => -1, 'post_type' => 'post', 'category_name'=> 'our_services');
    $posts = new WP_Query($args);
?>


<?php $__env->startSection('content'); ?>
<?php while(have_posts()): ?> <?php the_post() ?>
<section class="blog inheritance our-services page single-page">
    <div class="head-slider">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 text-center">
                    <h2 class="section-title"><?php echo get_the_title(); ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="main-layout v2">
        <div class="swiper-container">
            <div class="row-items row">
                <?php while($posts->have_posts()): ?> <?php $posts->the_post() ?>
                <div class="col-item swiper-slide col-12 col-md-6 col-lg-4">
                    <div class="item-wrap">
                        <div class="item-img">
                             <?php echo e(the_post_thumbnail('blog', array('class' => 'img-fluid lazyload', 'title' => get_the_title()))); ?>

                        </div>
                        <div class="item-title">
                            <h3><?php echo get_the_title(); ?></h3>
                        </div>
                        <div class="item-content">
                            <div class="post-content"><?php echo get_the_content(); ?></div>
                            <span class="date"><?php the_time('F d, Y'); ?></span>
                            <a href="<?php echo e(get_permalink()); ?>" class="no-btn">Read more</a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>