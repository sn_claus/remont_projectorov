<section class="troubles">
    <div class="container">
        <div class="row row-troubles">
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper">
                    <div class="item-title">
                        <a href="#" class="tri-title triafter tri-small">Не включается</a>
                    </div>
                    <div class="item-body">
                        <p>Прошивка ПО</p>
                        <p>Ремонт системной платы</p>
                        <p>Ремонт блока питания</p>
                    </div>
                    <div class="item-actions">
                        <a class="btn default" href="#">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                        <span class="item-price">от 000 Р</span>
                    </div>
                    <div class="item-count">01</div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper">
                    <div class="item-title">
                        <a href="#" class="tri-title triafter tri-small">Не включается</a>
                    </div>
                    <div class="item-body">
                        <p>Прошивка ПО</p>
                        <p>Ремонт системной платы</p>
                        <p>Ремонт блока питания</p>
                    </div>
                    <div class="item-actions">
                        <a class="btn default" href="#">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                        <span class="item-price">от 000 Р</span>
                    </div>
                    <div class="item-count">01</div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper">
                    <div class="item-title">
                        <a href="#" class="tri-title triafter tri-small">Не включается</a>
                    </div>
                    <div class="item-body">
                        <p>Прошивка ПО</p>
                        <p>Ремонт системной платы</p>
                        <p>Ремонт блока питания</p>
                    </div>
                    <div class="item-actions">
                        <a class="btn default" href="#">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                        <span class="item-price">от 000 Р</span>
                    </div>
                    <div class="item-count">01</div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper">
                    <div class="item-title">
                        <a href="#" class="tri-title triafter tri-small">Не включается</a>
                    </div>
                    <div class="item-body">
                        <p>Прошивка ПО</p>
                        <p>Ремонт системной платы</p>
                        <p>Ремонт блока питания</p>
                    </div>
                    <div class="item-actions">
                        <a class="btn default" href="#">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                        <span class="item-price">от 000 Р</span>
                    </div>
                    <div class="item-count">01</div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper">
                    <div class="item-title">
                        <a href="#" class="tri-title triafter tri-small">Не включается</a>
                    </div>
                    <div class="item-body">
                        <p>Прошивка ПО</p>
                        <p>Ремонт системной платы</p>
                        <p>Ремонт блока питания</p>
                    </div>
                    <div class="item-actions">
                        <a class="btn default" href="#">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                        <span class="item-price">от 000 Р</span>
                    </div>
                    <div class="item-count">01</div>
                </div>
            </div>
        </div>
    </div>
</section>