<section class="offers" id="offers">
    <?php $__env->startComponent('components.section-title', ['title' => 'offers', 'subtitle' => 'Fits perfectly']); ?>
    <?php echo $__env->renderComponent(); ?>
    <div class="offers-content">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center" data-aos="fade-up" data-aos-duration="800" data-aos-delay="400" data-aos-once="true">
                    <?php echo $offers->text; ?>

                </div>
            </div>
            <div class="row">
                <div class="col-xl-10 offset-xl-1 col-12 swiper-container">
                    <div class="row swiper-wrapper" data-aos="columns" data-aos-once="true">
                    <?php $__currentLoopData = $offers->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="item col-lg-4 col-12 swiper-slide" data-aos="fade-left" data-aos-duration="800" data-aos-delay=<?php echo e($key*400); ?> data-aos-once="true">
                            <div class="item-wrap">
                                <?php if($item['best']): ?>
                                    <div class="item-badge" data-aos data-aos-delay="4s">
                                        <div class="badge-wrapper">
                                            <span class="front">
                                                <span class="content"><?php echo e(pll__('Best choice')); ?></span>
                                            </span>
                                            <span class="back"></span>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <h3 class="item-title"><?php echo e($item['title']); ?></h3>
                                <div class="img-wrap">
                                    <img src="#" data-imgindex="<?php echo e($item['title']); ?>" data-src="<?php echo e($item['img']); ?>" class="img-fluid lazyload item<?php echo $key; ?>" alt="offer item">
                                    <div class="swiper-lazy-preloader"></div>
                                </div>
                                <div class="price-group">
                                    <span class="currency">€</span>
                                    <span class="value"><?php echo $item['price']; ?></span>
                                    <span class="month">/ <?php echo e(pll__('month')); ?></span>
                                </div>
                                <div class="item-feature">
                                    <?php $__currentLoopData = $item['features']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="single-feature">
                                        <?php if($feature['check']): ?>
                                            <img src="#" alt="true" class="lazyload img-fluid" data-src="<?= App\asset_path('images/true.png'); ?>">
                                        <?php else: ?>
                                            <img src="#" alt="false" class="lazyload img-fluid" data-src="<?= App\asset_path('images/false.png'); ?>">
                                        <?php endif; ?>
                                        <span><?php echo $feature['title']; ?></span>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <div class="item-action">
                                    <button class="btn blue" data-choice="<?php echo e($item['title']); ?>" data-micromodal-trigger="order"><?php echo e(pll__('Order')); ?></button>
                                </div>
                            </div>
                            
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</section>