

<div class="dropdown">
    <input type="hidden" name=<?php echo e($name); ?>>
    <button class="dropdown-toggle" type="button">
        <span class="choice"><?php echo e($placeholder); ?></span>
        
    </button>
    <div class="dropdown-menu position-absolute d-none scroll-container transparent">
        <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <button class="dropdown-item" type="button" data-value=<?php echo e($item); ?>><?php echo e($item); ?></button>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>