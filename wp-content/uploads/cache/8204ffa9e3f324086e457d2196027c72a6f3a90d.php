<section class="about-us" id="about_us">
    <?php $__env->startComponent('components.section-title', ['subtitle' => 'about us', 'icon' => true]); ?>
    <?php echo $__env->renderComponent(); ?>
    <div class="about-content">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-md-6 col-12 about-column">
                    <div data-aos="fade-left" data-aos-duration="800" data-aos-delay="600" data-aos-once="true"><?php echo $about_us->text; ?></div>
                    <button data-aos="fade" data-aos-duration="800" data-aos-delay="800" data-aos-once="true" class="btn-play no-btn my-auto" data-micromodal-trigger="play">
                        <div class="play-wrap btn">
                            <img src="#" alt="play" class="img-fluid lazyload" data-src="<?= App\asset_path('images/play.png'); ?>">  
                            <div class="swiper-lazy-preloader"></div>
                        </div>
                        <span class="text-btn"><?php echo e(pll__('Play the video')); ?></span> 
                    </button>
                </div>
                <div class="col-xl-7 col-md-6 col-12">
                    <img src="#" data-aos="fade-right" data-aos-duration="800" data-aos-delay="200" data-aos-once="true" class="img-fluid lazyload" alt="about-us" data-src="<?= App\asset_path('images/about_us.png'); ?>">
                    <div class="swiper-lazy-preloader"></div>
                </div>
            </div>
        </div>
    </div>
</section>