<?php $phone = preg_replace('/\D/', '', get_field('phone','options')) ?>
<footer>
  <div class="container">
    <div class="row row-main">
      <div class="col-lg-auto col-12 col-md-3">
        <div class="logo-wrap d-none d-md-block">
            <img alt="logo-hnj" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/logo.png">
        </div>
        <div class="social-wrap">
          <ul class="unstyled">
            <?php if(get_field('facebook', 'options')): ?>
            <li>
            <a rel="nofollow" class="social-link" href="<?php echo e(get_field('facebook', 'options')); ?>">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <?php endif; ?>
            <?php if(get_field('twitter', 'options')): ?>
            <li>
              <a rel="nofollow" class="social-link" href="<?php echo e(get_field('twitter', 'options')); ?>">
                  <i class="fab fa-twitter"></i>
              </a>
            </li>
            <?php endif; ?>
            <?php if(get_field('insta', 'options')): ?>
            <li>
              <a rel="nofollow" class="social-link" href="<?php echo e(get_field('insta', 'options')); ?>">
                <i class="fab fa-instagram"></i>
              </a>
            </li>
            <?php endif; ?>
          </ul>
        </div>
      </div>
      <div class="main-nav col d-none d-md-block">
        <?php if(has_nav_menu('primary_navigation')): ?>
          <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav','walker' => new web_walker]); ?>

        <?php endif; ?>
      </div>
      <div class="col-info text-center col-12 col-xl-2 col-md-3 text-md-right">
      <a class="link-tel" href="tel:<?php echo $phone; ?>"><?php echo e(get_field('phone','options')); ?></a>
        <span><?php echo e(get_field('cord','options')); ?></span>
      </div>
    </div>
    <div class="row row-copy">
      <div class="col-12 text-center">
        <span>© Copyright 2019<?php echo get_field('copy', 'options') ? ' - '.get_field('copy', 'options') : ''; ?></span>
      </div>
    </div>
  </div>
  <div class="nav-mobile">
      <?php if(has_nav_menu('primary_navigation')): ?>
        <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'items_wrap' => '<ul id="%1$s" class="nav %2$s">%3$s</ul>', 'container'=> false,'walker' => new web_walker]); ?>

      <?php endif; ?>
    </div>
</footer>
<div class="modal fade" id="gallery" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="slider-layout">
        <div class="swiper-container main">
          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">
              <!-- Slides -->
              <div class="swiper-slide">
                <img alt="alt-img" class="img-fluid lazyload" data-src="<?= get_template_directory_uri(); ?>/assets/images/work2.jpg">
              </div>
              <div class="swiper-slide">
                <img alt="alt-img" class="img-fluid lazyload" data-src="<?= get_template_directory_uri(); ?>/assets/images/work2.jpg">
              </div>
              <div class="swiper-slide">
                  <img alt="alt-img" class="img-fluid lazyload" data-src="<?= get_template_directory_uri(); ?>/assets/images/work2.jpg">
              </div>
          </div>
        </div>
        <div class="swiper-container thumbs">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">
                  <img alt="alt-img" class="img-fluid lazyload" data-src="<?= get_template_directory_uri(); ?>/assets/images/work2.jpg">
                </div>
                <div class="swiper-slide">
                  <img alt="alt-img" class="img-fluid lazyload" data-src="<?= get_template_directory_uri(); ?>/assets/images/work2.jpg">
                </div>
            </div>
            <div class="swiper-scrollbar"></div>
          </div>
        </div>
        <div class="add-info">
          <div class="add-title">
            <h3>Bathroom</h3>
          </div>
          <div class="tag-content">
            <div class="tag-item">
              <i class="fal fa-calendar-alt"></i>
              <span>tag desc</span>
            </div>
            <div class="tag-item">
              <i class="fal fa-calendar-alt"></i>
              <span>tag desc</span>
            </div>
            <div class="tag-item">
              <i class="fal fa-calendar-alt"></i>
              <span>tag desc</span>
            </div>
              <div class="tag-item">
                <i class="fal fa-calendar-alt"></i>
                <span>tag desc</span>
              </div>
          </div>
          <div class="add-content">
            <h3 class="content-title">Our Solutions</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $review_form = get_field('testimonial_form', 'options') ?>
<div class="modal fade" id="testimonial" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <?php echo do_shortcode('[contact-form-7 id="'.$review_form->ID.'" title="'.$review_form->post_title.'"]'); ?>

        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="ok" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h3 class="py-3 m-auto">Thanks for your feedback!</h3>
          <div class="form-group text-center mt-5 w-100">
            <button type="button" class="btn">OK</button>
          </div>
        </div>
      </div>
    </div>
</div>