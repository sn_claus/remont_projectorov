<?php 
    $parent_id = get_field('get_a_quote', 'options');
    $posts = get_field('review', 'options');
?>

<section class="testimonials inheritance our-services">
    <div class="head-slider">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 text-center">
                    <h2 class="section-title">Testimonials</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="main-layout v3">
        <div class="swiper-container">
            <div class="row-items swiper-wrapper">
                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <div class="col-item swiper-slide">
                    <div class="item-wrap testimony">
                        <div class="item-title whois">
                            <h3 class="name"><?php echo e($post['name']); ?></h3>
                        <span class="role"><?php echo e($post['role']); ?></span>
                        </div>
                        <div class="item-content tellus">
                            <p><?php echo e($post['text']); ?></p>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev main v3"></div>
        <div class="swiper-button-next main v3"></div>
    </div>
    <div class="row-actions">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <a href="<?php echo e(get_permalink($parent_id)); ?>" data-toggle="modal" data-target="#testimonial" class="no-btn v2">Leave a testimonial <i class="far fa-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>