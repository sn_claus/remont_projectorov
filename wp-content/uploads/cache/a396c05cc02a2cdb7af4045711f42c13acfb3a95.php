
<div class="section-title">
    <div class="aos-controller">
        <span class="subtitle d-block" data-aos="slide-up" data-aos-duration="800" data-aos-delay="800" data-aos-once="true"><?php echo e(pll__($subtitle)); ?></span>
    </div>
        <?php if($icon): ?>
            <div class="maintitle" data-aos>
                <img src="#" data-aos="fade-up" data-aos-duration="800" data-aos-delay="200" data-aos-once="true" class="img-fluid lazyload" data-src="<?= App\asset_path('images/logo.png'); ?>">
            </div>
        <?php else: ?>
            <div class="aos-controller">
                <h2 class="maintitle" data-aos="slide-up" data-aos-duration="800" data-aos-delay="200" data-aos-once="true"><?php echo e(pll__($title)); ?></h2>
            </div>
        <?php endif; ?>
</div>