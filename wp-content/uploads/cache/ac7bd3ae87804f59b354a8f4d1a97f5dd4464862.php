<?php $__env->startSection('content'); ?>
  <?php while(have_posts()): ?> <?php the_post() ?>
    <?php echo $__env->make('partials.content-single-'.get_post_type(), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="relative-content">
        <?php
          $args = array('posts_per_page' => 3, 'orderby' => 'rand', 'post_type' => 'post', 'category_name'=> 'blog','post__not_in' => array( get_the_ID() ));
          $posts = new WP_Query($args);
        ?>
        <section class="blog inheritance our-services">
          <div class="head-slider related-aticles">
              <div class="container">
                  <div class="row row-header">
                      <div class="col-12 text-center">
                          <h2 class="section-title before">Related Articles</h2>
                      </div>
                  </div>
              </div>
          </div>
          <div class="main-layout v2">
              <div class="swiper-container container">
                  <div class="row-items swiper-wrapper">
                      <?php while($posts->have_posts()): ?> <?php $posts->the_post() ?>
                      <div class="col-item swiper-slide">
                          <div class="item-wrap">
                              <div class="item-img">
                                  <?php echo e(the_post_thumbnail('blog', array('class' => 'img-fluid lazyload', 'title' => get_the_title()))); ?>

                                  <div class="swiper-lazy-preloader"></div>
                              </div>
                              <div class="item-title">
                                <h3><?php echo e(get_the_title()); ?></h3>
                              </div>
                              <div class="item-content">
                                  <span class="date"><?php the_time('F d, Y'); ?></span>
                                  <a href="<?php echo e(get_permalink()); ?>" class="no-btn">Read more</a>
                              </div>
                          </div>
                      </div>
                      <?php endwhile; ?>
                  </div>
              </div>
              <div class="swiper-button-prev main v2"></div>
              <div class="swiper-button-next main v2"></div>
          </div>
        </section>
      </div>
  <?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>