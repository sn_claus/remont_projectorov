<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php wp_head() ?>
  <link rel="shortcut icon" href="<?php echo get_field('favicon', 'options')['sizes']['fav']; ?>" type="image/png">
</head>
