<?php 
    $parent_id = get_field('blog', 'options');
    $form = get_field('quote_form', 'options')
?>


<?php $__env->startSection('content'); ?>
<?php while(have_posts()): ?> <?php the_post() ?>
<section class="get-quote inherited page single-page contactus">
    <div class="head-slider">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 text-center">
                    <h1 class="section-title before main-title">Get Your Instant Quote Now!</h1>
                </div>
            </div>
        </div>
    </div>
    <div class='main-content'>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 col-lg-6 form-init">
                    <?php echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); ?>

                </div>
                <div class="col-12 col-md-5 col-lg-6 d-flex flex-wrap">
                    <div class="email-img d-none d-md-block my-auto">
                        <img alt="contact us" src="#" class="lazyload img-fluid" data-src="<?php echo e(get_template_directory_uri()); ?>/assets/images/quote1.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php endwhile; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>