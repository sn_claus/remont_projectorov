<section class="promotion">
    <div class="container">
        <div class="row">
            <div class="col-12 promo-wrapper">
                <h3 class="promo-title">Успей оставить заявку</h3>
                <div class="promo-body">
                    <div class="timer">
                        <span class="digits">00:00:00</span>
                    </div>
                </div>
                <h3 class="promo-title-after">и получи скидку <b>500 рублей</b></h3>
            </div>
        </div>
    </div>
</section>