
<?php 
  $args = array('posts_per_page' => -1, 'post_type' => 'post', 'category_name'=> 'our_works');
    $posts = new WP_Query($args);
?>


<?php $__env->startSection('content'); ?>
<?php while(have_posts()): ?> <?php the_post() ?>
<section class="our-works page single-page">
    <div class="head-slider">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 text-center">
                    <h1 class="section-title before main-title">Our Works</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="gallery-slider mobile-only">
        <div class="container">
            <div class="row-items row">
                    
                    <?php while($posts->have_posts()): ?> <?php $posts->the_post() ?>
                    <div class="col-item col-md-4 col-sm-6 col-xs-12 swiper-slide">
                    <div class="img-wrap" data-toggle="modal" data-id="<?php echo e(the_ID()); ?>" data-target="#gallery">
                            <?php echo e(the_post_thumbnail('our-works', array('class' => 'img-fluid lazyload', 'title' => get_the_title()))); ?>

                            <i class="fal fa-search"></i>
                            <div class="work-about">
                                <h2><?php echo e(the_title()); ?></h2>
                            <span><?php echo e(get_field('desc')); ?></span>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>