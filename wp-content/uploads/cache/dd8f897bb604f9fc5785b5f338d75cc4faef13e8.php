<footer class="floor">
  <div class="container d-none d-xl-flex">
    <nav class="main-nav">
        <a class="logo-wrap" href="#">
            <img alt="logo-main" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/logo.png">
            <span class="logo-text">ремонт <b>проекторов</b></span>
        </a>
        <div class="nav-menu-toggler d-xl-none order-3">
          <button type="button" class="toggle-nav">
            <span></span>
            <span class="less"></span>
            <span class="less"></span>
            <span></span>
          </button>
          </div>
        <div class="nav nav-menu d-none d-xl-block">
          <?php if(has_nav_menu('primary_navigation')): ?>
            <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new web_walker]); ?>

          <?php endif; ?>
        </div>
        <a href="#" class="btn light">Заказать ремонт 
            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
            <?php echo $__env->renderComponent(); ?>
        </a>
      </nav>
  </div>
  <div class="nav-mobile">
      <?php if(has_nav_menu('primary_navigation')): ?>
        <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'items_wrap' => '<ul id="%1$s" class="nav %2$s">%3$s</ul>', 'container'=> false,'walker' => new web_walker]); ?>

      <?php endif; ?>
      <div class="logo-wrap d-none d-md-block">
        <img alt="logo-mobile" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/logo.png">
      </div>
    </div>
    <div class="other-stuff"></div>
      <div class="micro_modal" id="demo" aria-hidden="true">
          <div class="modal_overlay d-flex justify-content-center align-items-center" tab-index="-1" data-micromodal-close>
            <div class="modal_container" role="dialog" aria-modal="true" aria-labelledby="demo">
              <header class="modal-header d-flex justify-content-start align-items-center">
                <button class="modal-close toggle-nav opened" aria-label="Close modal" data-micromodal-close >
                    <span class="shape d-none"></span>
                    <span class="shape"></span>
                    <span class="shape"></span>
                    <span class="shape d-none"></span>
                </button>
              </header>
                <div class="modal-content">
                </div>
              </div>
            </div>
        </div>
        <div class="mdc-snackbar mdc-snackbar--leading">
            <div class="mdc-snackbar__surface">
              <div class="mdc-snackbar__label" role="status" aria-live="polite">
              </div>
            </div>
        </div>
    </div>
</footer>