<section class="benefits">
    <div class="container">
        <div class="row ben-row">
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper triafter">
                    <div class="item-title">
                        <h2 class="tri-title multi">Ремонт <b>в день обращения</b></h2>
                    </div>
                    <div class="item-body linebefore">
                        <p>Мы понимаем, как ценно Ваше время, поэтому предоставляем услугу ремонта проектора в день обращения. Не важно, что случилось с Вашим проектора: не включается или не выключается – мы занимаемся ремонтом уже более 8 лет, а потому нам не составит труда исправить поломку в кратчайшие сроки.</p>
                    </div>
                    <div class="item-actions">
                        <a href="<?php echo get_permalink(); ?>" class="btn default">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                    </div>
                    <div class="item-bg">
                        <img alt="item-bg" class="img-fluid" src="<?= get_template_directory_uri(); ?>/assets/images/benbg.png">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper triafter">
                    <div class="item-title">
                        <h2 class="tri-title multi">Ремонт <b>в день обращения</b></h2>
                    </div>
                    <div class="item-body linebefore">
                        <p>Мы понимаем, как ценно Ваше время, поэтому предоставляем услугу ремонта проектора в день обращения. Не важно, что случилось с Вашим проектора: не включается или не выключается – мы занимаемся ремонтом уже более 8 лет, а потому нам не составит труда исправить поломку в кратчайшие сроки.</p>
                    </div>
                    <div class="item-actions">
                        <a href="<?php echo get_permalink(); ?>" class="btn default">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                    </div>
                    <div class="item-bg">
                        <img alt="item-bg" class="img-fluid" src="<?= get_template_directory_uri(); ?>/assets/images/benbg.png">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper triafter">
                    <div class="item-title">
                        <h2 class="tri-title multi">Ремонт <b>в день обращения</b></h2>
                    </div>
                    <div class="item-body linebefore">
                        <p>Мы понимаем, как ценно Ваше время, поэтому предоставляем услугу ремонта проектора в день обращения. Не важно, что случилось с Вашим проектора: не включается или не выключается – мы занимаемся ремонтом уже более 8 лет, а потому нам не составит труда исправить поломку в кратчайшие сроки.</p>
                    </div>
                    <div class="item-actions">
                        <a href="<?php echo get_permalink(); ?>" class="btn default">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                    </div>
                    <div class="item-bg">
                        <img alt="item-bg" class="img-fluid" src="<?= get_template_directory_uri(); ?>/assets/images/benbg.png">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper triafter">
                    <div class="item-title">
                        <h2 class="tri-title multi">Ремонт <b>в день обращения</b></h2>
                    </div>
                    <div class="item-body linebefore">
                        <p>Мы понимаем, как ценно Ваше время, поэтому предоставляем услугу ремонта проектора в день обращения. Не важно, что случилось с Вашим проектора: не включается или не выключается – мы занимаемся ремонтом уже более 8 лет, а потому нам не составит труда исправить поломку в кратчайшие сроки.</p>
                    </div>
                    <div class="item-actions">
                        <a href="<?php echo get_permalink(); ?>" class="btn default">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                    </div>
                    <div class="item-bg">
                        <img alt="item-bg" class="img-fluid" src="<?= get_template_directory_uri(); ?>/assets/images/benbg.png">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper triafter">
                    <div class="item-title">
                        <h2 class="tri-title multi">Ремонт <b>в день обращения</b></h2>
                    </div>
                    <div class="item-body linebefore">
                        <p>Мы понимаем, как ценно Ваше время, поэтому предоставляем услугу ремонта проектора в день обращения. Не важно, что случилось с Вашим проектора: не включается или не выключается – мы занимаемся ремонтом уже более 8 лет, а потому нам не составит труда исправить поломку в кратчайшие сроки.</p>
                    </div>
                    <div class="item-actions">
                        <a href="<?php echo get_permalink(); ?>" class="btn default">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                    </div>
                    <div class="item-bg">
                        <img alt="item-bg" class="img-fluid" src="<?= get_template_directory_uri(); ?>/assets/images/benbg.png">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="item-wrapper triafter">
                    <div class="item-title">
                        <h2 class="tri-title multi">Ремонт <b>в день обращения</b></h2>
                    </div>
                    <div class="item-body linebefore">
                        <p>Мы понимаем, как ценно Ваше время, поэтому предоставляем услугу ремонта проектора в день обращения. Не важно, что случилось с Вашим проектора: не включается или не выключается – мы занимаемся ремонтом уже более 8 лет, а потому нам не составит труда исправить поломку в кратчайшие сроки.</p>
                    </div>
                    <div class="item-actions">
                        <a href="<?php echo get_permalink(); ?>" class="btn default">
                            <?php $__env->startComponent('components.icon', ['name' => 'arrow']); ?>
                            <?php echo $__env->renderComponent(); ?> <span>Подробнее</span>
                        </a>
                    </div>
                    <div class="item-bg">
                        <img alt="item-bg" class="img-fluid" src="<?= get_template_directory_uri(); ?>/assets/images/benbg.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>