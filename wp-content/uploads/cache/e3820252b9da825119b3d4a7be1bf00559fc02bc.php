

<div class="dropdown">
    <input type="hidden" name=<?php echo e($name); ?>>
    <button class="dropdown-toggle btn light small" type="button">
        <span class="choice"><?php echo e($placeholder); ?></span>
        
    </button>
    <div class="dropdown-menu position-absolute d-none scroll-container transparent">
        <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($placeholder == $item['name']): ?>
                <span class="dropdown-item active dissabled"><?php echo e($item['name']); ?></span>
            <?php else: ?>
                <a href=<?php echo e($item['url']); ?> class="dropdown-item" data-value=<?php echo e($item['name']); ?>><?php echo e($item['name']); ?></a>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>