<?php 
    $parent_id = get_field('get_a_quote', 'options');
    $args = array('posts_per_page' => 3, 'post_type' => 'post', 'category_name'=> 'our_services');
    $posts = new WP_Query($args);
?>

<section class="our-services webvision">
    <div class="head-slider">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 text-center">
                    <h2 class="section-title">Our Services</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="main-layout v1">
        <div class="swiper-container">
            <div class="row-items row">
                <?php while($posts->have_posts()): ?> <?php $posts->the_post() ?>
                <div class="col-item swiper-slide col-lg-4 col-12 col-sm-6">
                    <div class="item-wrap">
                        <div class="item-img">
                             <?php echo the_post_thumbnail('our-service', array('class' => 'img-fluid lazyload', 'title' => get_the_title())); ?>

                            <div class="swiper-lazy-preloader"></div>
                        </div>
                        <div class="item-title">
                            <h3><?php echo get_the_title(); ?></h3>
                        </div>
                        <div class="item-content">
                            <p><?php echo get_excerpt(100, 'post_content'); ?></p>
                        </div>
                        <a href="<?php echo get_permalink($parent_id); ?>" class="no-btn">Get a quote</a>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>