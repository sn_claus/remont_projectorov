<header class="main-head">
  <div class="container">
  <nav class="main-nav">
    <div class="logo-wrap">
        <img alt="logo-main" class="logo-img lazyload" src="#" data-src="<?= App\asset_path('images/logo_white.png'); ?>">
        <div class="swiper-lazy-preloader"></div>
    </div>
    <div class="nav-menu-toggler d-md-none">
        <button type="button" class="toggle-nav">
          <span></span>
          <span class="less"></span>
          <span class="less"></span>
          <span></span>
        </button>
      </div>
    <div class="nav nav-menu d-none d-md-block">
      <?php if(has_nav_menu('primary_navigation')): ?>
        <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new web_walker]); ?>

      <?php endif; ?>
    </div>
    <div class="nav lang-switcher d-none d-md-block">
      <?php $__env->startComponent('components.langswitcher', ['name' => 'langswitcher','placeholder' => $current_lang, 'items' => $langs]); ?>
      <?php echo $__env->renderComponent(); ?>
    </div>
  </nav>
</div>
</header>
