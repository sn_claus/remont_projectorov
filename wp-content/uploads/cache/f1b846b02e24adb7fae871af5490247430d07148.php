<?php 
    $parent_id = get_field('our_works', 'options');
    $posts = get_field('slides');
    $posts_desc = get_field('slides_desc');
?>

<section class="home-slider">
    <div class="swiper-container">
        <div class="swiper-wrapper">
          <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="swiper-slide <?php echo e($post['inverse'] ? 'inverse' : ''); ?>">
            <div class="slider-img swiper-lazy" data-background="<?php echo e($post['image']); ?>"></div>
                <div class="swiper-lazy-preloader"></div>
            <div class="slider-desc"><?php echo e($posts_desc); ?></div>
                <div class="slider-content">
                <h2 class="slide-title" data-swiper-parallax="-150%"><?php echo e($post['title']); ?></h2>
                <span class="slide-text" data-swiper-parallax="-150%"><?php echo e($post['text']); ?></span>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="swiper-button-prev swiper-button-white"></div>
        <div class="swiper-button-next swiper-button-white"></div>
    </div>
</section>