<section class="trade-tax" id="trade_tax" >
    <?php $__env->startComponent('components.section-title', ['title' => 'Save business tax', 'subtitle' => 'tax']); ?>
    <?php echo $__env->renderComponent(); ?>
    <div class="about-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12 pr-md-5">
                        <div data-aos="fade-left" data-aos-duration="800" data-aos-delay="500" data-aos-once="true"><?php echo $tax->text; ?></div>
                        <img src="#" data-aos="fade" data-aos-duration="800" data-aos-delay="700" data-aos-once="true" class="img-fluid lazyload mt-4  d-none d-md-block" alt="about-us" data-src="<?= App\asset_path('images/tax-img.png'); ?>">
                        <div class="swiper-lazy-preloader hidden-max-sm"></div>
                    </div>
                    <div class="col-md-6 col-12 mt-5 mt-md-0">
                      <div class="form-wrap" data-aos="fade-right" data-aos-duration="800" data-aos-delay="200" data-aos-once="true">
                          <div class="form-head">
                              <h3 class="form-title"><?php echo e(pll__('Tax Calculator')); ?></h3>
                          </div>
                          <div class="form-body form-init">
                            <div class="form-group input-wrap">
                                <input class="form-control" name="commercial" id="form_inp">
                                <label class="lab-wrap">
                                        <?php echo e(pll__('Commercial income')); ?> €
                                </label>
                            </div>
                            <div class="form-group input-wrap">
                                <input class="form-control" name="commercial" id="form_inp2">
                                <label class="lab-wrap">
                                        <?php echo e(pll__('Trade tax rate')); ?> %
                                </label>
                            </div>
                            <div class="radio-group">
                                <h3 class="radio-title"><?php echo e(pll__('Allowances')); ?></h3>
                                <div class="mdc-form-field w-100">
                                    <div class="mdc-radio">
                                        <input class="mdc-radio__native-control" value="first" type="radio" id="radio-1" name="radios">
                                        <div class="mdc-radio__background">
                                        <div class="mdc-radio__outer-circle"></div>
                                        <div class="mdc-radio__inner-circle"></div>
                                        </div>
                                    </div>
                                    <label for="radio-1"><?php echo e(pll__('Sole proprietorship / partnerships')); ?></label>
                                </div>
                                <div class="mdc-form-field w-100">
                                    <div class="mdc-radio">
                                        <input class="mdc-radio__native-control" value="second" type="radio" id="radio-2" name="radios">
                                        <div class="mdc-radio__background">
                                        <div class="mdc-radio__outer-circle"></div>
                                        <div class="mdc-radio__inner-circle"></div>
                                        </div>
                                    </div>
                                    <label for="radio-2"><?php echo e(pll__('Corporations')); ?></label>
                                </div>
                                <div class="mdc-form-field w-100">
                                    <div class="mdc-radio">
                                        <input class="mdc-radio__native-control" value="third" type="radio" id="radio-3" name="radios">
                                        <div class="mdc-radio__background">
                                        <div class="mdc-radio__outer-circle"></div>
                                        <div class="mdc-radio__inner-circle"></div>
                                        </div>
                                    </div>
                                    <label for="radio-3"><?php echo e(pll__('Legal persons of public law e.g. Clubs')); ?></label>
                                </div>
                                <div class="form-action">
                                    <button type="submit" class="btn blue" data-micromodal-trigger="calculate" disabled><?php echo e(pll__('Send')); ?></button>
                                </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
</section>